FROM mcr.microsoft.com/mssql/server:2017-CU20-ubuntu-16.04

RUN mkdir -p /usr/config
WORKDIR /usr/config

COPY Database /usr/config
COPY entrypoint.sh /usr/config
COPY configure-db.sh /usr/config

# Grant permissions for to our scripts to be executable
RUN chmod +x /usr/config/entrypoint.sh
RUN chmod +x /usr/config/configure-db.sh

ENTRYPOINT ["./entrypoint.sh"]
CMD ["true"]
