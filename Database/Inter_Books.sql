use master
go
DROP database if exists library_ecommerce
go
create database library_ecommerce
go

use library_ecommerce
go

create table people
(
	uuid char(36) not null primary key,
	people_name varchar(50) not null,
	cpf char(11) not null unique, 
	birth date not null,
	people_login varchar(25) not null unique,
	people_password varchar(max) not null,
	photo varchar(max),
	created_at datetime,
	updated_at datetime,
	deleted_at datetime
)
go

create table readers
(
	people_uuid char(36) not null primary key references people,
	telephone varchar(11),
	email varchar(50) not null unique,
	about varchar(max),
	created_at datetime,
	updated_at datetime,
	deleted_at datetime
)
go


create table employees
(
	people_uuid char(36) not null primary key references people,
	employee_type varchar(13) NOT NULL CHECK
	(employee_type   IN ('administrador', 'colaborador')),

	employee_status  varchar(9) NOT NULL CHECK
	(employee_status   IN ('ativo', 'demitido', 'suspenso')),
	created_at datetime,
	updated_at datetime,
	deleted_at datetime
)
go


create table delivery_addresses
(
	uuid char(36) not null primary key,
	reader_people_uuid char(36) not null references readers ,
	number varchar(10) not null,
	cep varchar(8) not null,
	address varchar(50) not null,
	neighborhood varchar(50) not null,
	city varchar(50) not null,
	complement varchar(50),
	state_address char(2) not null CHECK
	(state_address IN ('AC', 'AL', 'AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PA','PB','PR','PE','PI','RJ','RN','RS','RO','RR','SC','SP','SE','TO')),
	created_at datetime,
	updated_at datetime,
	deleted_at datetime
)
go


create table books
(
	uuid char(36) not null primary key,
	employee_people_uuid char(36) not null references employees,
	name varchar(50) not null,
	author varchar(50) not null,
	pages varchar(5) not null,
	publishing_company varchar(50) not null,
	quantity int not null,
	genre varchar(20) not null,
	price money not null,
	isbn varchar(20) unique,
	cover varchar(max),
	synopsis nvarchar(max),
	created_at datetime,
	updated_at datetime,
	deleted_at datetime
)
go

create table carts
(
	uuid char(36) not null primary key,
	delivery_addresses_uuid char(36) not null references delivery_addresses,
	bought_in datetime not null,
	status_carts varchar(10) NOT NULL CHECK (status_carts IN ('aguardando', 'finalizado')),
	created_at datetime,
	updated_at datetime
)
go

create table items_cart
(
	cart_uuid char(36) not null references carts,
	book_uuid char(36) not null references books,
	quantity int not null,
	price money not null,
	primary key(cart_uuid, book_uuid)
)
go

create function totalOfCart(@cart_uuid char(36)) 
returns money
as
begin
    return (select SUM(quantity * price) from items_cart where items_cart.cart_uuid = @cart_uuid)
end
go

alter table dbo.carts ADD total As dbo.totalOfCart(uuid)
go