use library_ecommerce
go 


--executando as seeds--

-- 1 leitor --
Exec upsertReader  
	@uuid='182ba040-0f49-11eb-adc1-0242ac120002',
	@people_name = 'Gama',
	@cpf ='32431123030',
	@birth ='2001-10-27',
	@people_login = 'gama123',
	@people_password = '132',
	@photo = '',
	@telephone ='135674289',
	@email ='gama@hotmail.com',
	@about = ''

go

-- 1 funcion�rio administrador --
Exec upsertEmployee
    @uuid = 'd8de96c6-0f49-11eb-adc1-0242ac120002',
    @people_name = 'Ellon',
    @cpf = '05844121007',
    @birth = '1970-07-12',
	@photo = '',
    @people_login = 'el123',
    @people_password = '456',
    @telephone = '987521443',
    @email = 'ellonEl@hotmail.com',
	@about= 'ol�',

    @employee_type = 'administrador',
    @employee_status = 'ativo'

go


-- 3 funcion�rios comuns --
Exec upsertEmployee
    @uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
    @people_name = 'Keyse',
    @cpf = '36487143090',
    @birth = '2002-11-13',
	@photo = '',
    @people_login = 'key123',
    @people_password = '123',
    @telephone = '997634210',
    @email = 'key@hotmail.com',
	@about= 'Teste',
	
    @employee_type = 'colaborador',
    @employee_status = 'ativo'

go

Exec upsertEmployee
    @uuid = 'd63c860c-0f4a-11eb-adc1-0242ac120002',
    @people_name = 'Giwe',
    @cpf = '85565346002',
    @birth = '2004-09-20',
	@photo = '',
    @people_login = 'gew123',
    @people_password = '456',
    @telephone = '9912567384',
    @email = 'gew@hotmail.com',
	@about= 'empregadinho',

    @employee_type = 'colaborador',
    @employee_status = 'demitido'

go

Exec upsertEmployee
    @uuid = '33741272-0f4b-11eb-adc1-0242ac120002',
    @people_name = 'Durke',
    @cpf = '31144100062',
    @birth = '2001-03-02',
	@photo = '',
    @people_login = 'durk123',
    @people_password = '456',
    @telephone = '987563712',
    @email = 'durk@hotmail.com',
	@about= 'Empregados',

    @employee_type = 'colaborador',
    @employee_status = 'suspenso'

go

-- 1 Endereco -- 
Exec upsertAddress
	@uuid = '33e0469e-0f4c-11eb-adc1-0242ac120002',
	@reader_people_uuid = '182ba040-0f49-11eb-adc1-0242ac120002',
	@number = '874',
	@cep = '49030-650',
	@address = 'Rua Presb�tero Alo�sio Santos',
	@neighborhood = 'Farol�ndia',
	@city = 'Aracaju',
	@complement = '',
	@state_address = 'SE'

go

-- 4 livros --
Exec upsertBook
    @uuid = '7a6264d4-0f4d-11eb-adc1-0242ac125896',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Jogos Vorazes',
	@author = 'Suzanne Collins',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '400',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 20,
	@genre = 'Fic��o Cient�fica',
	@price = 27.00,
	@isbn = '8579800242',
	@synopsis = 'Ap�s o fim da Am�rica do Norte, uma nova na��o chamada Panem surge. Formada por doze distritos, � comandada com m�o de ferro pela Capital. Uma das formas com que demonstram seu poder sobre o resto do carente pa�s � com Jogos Vorazes, uma competi��o anual transmitida ao vivo pela televis�o, em que um garoto e uma garota de doze a dezoito anos de cada distrito s�o selecionados e obrigados a lutar at� a morte! Para evitar que sua irm� seja a mais nova v�tima do programa, Katniss se oferece para participar em seu lugar. Vinda do empobrecido distrito 12, ela sabe como sobreviver em um ambiente hostil. Peeta, um garoto que ajudou sua fam�lia no passado, tamb�m foi selecionado. Caso ven�a, ter� fama e fortuna. Se perder, morre. Mas para ganhar a competi��o, ser� preciso muito mais do que habilidade. At� onde Katniss estar� disposta a ir para ser vitoriosa nos Jogos Vorazes?'

go

Exec upsertBook
    @uuid = '7a6264d4-0f4d-11eb-adc1-0242ac120002',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Jogos Vorazes - Em chamas',
	@author = 'Suzanne Collins',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 16,
	@genre = 'Fic��o Cient�fica',
	@price = 34.90,
	@isbn = '8579800641',
	@synopsis = 'Depois do improv�vel e inusitado resultado dos �ltimos Jogos Vorazes, algo parece ter mudado para sempre em Panem. Aqui e ali, dist�rbios e agita��es nos distritos d�o sinais de que uma revolta � iminente. Katniss e Peeta, representantes do paup�rrimo Distrito 12, n�o apenas venceram os Jogos, mas ridicularizaram o governo e conseguiram fazer todos � incluindo o pr�prio Peeta � acreditarem que s�o um casal apaixonado. A confus�o na cabe�a de Katniss n�o � menos do que a das ruas. Em meio ao turbilh�o, ela pensa cada vez mais em seu melhor amigo, o jovem ca�ador Gale, mas � obrigada a fingir que o romance com Peeta � real. J� o governo parece especialmente preocupado com a influ�ncia que os dois adolescentes vitoriosos � transformados em verdadeiros �dolos nacionais � podem ter na popula��o. Por isso, existem planos especiais para mant�-los sob controle, mesmo que isso signifique for��-los a lutar novamente.'

go

Exec upsertBook
    @uuid = '1259ef59-7310-4330-b966-ea65c772dc74',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857980061',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'

go



Exec upsertBook
    @uuid = 'e5184bc2-0f4d-11eb-adc1-0242ac120002',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Jogos Vorazes - A esperan�a',
	@author = 'Suzanne Collins',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '424',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 30,
	@genre = 'Fic��o Cient�fica',
	@price = 34.90,
	@isbn = '8579800862',
	@synopsis = 'Depois de sobreviver duas vezes � crueldade de uma arena projetada para destru�-la, Katniss acreditava que n�o precisaria mais de lutar. Mas as regras do jogo mudaram: com a chegada dos rebeldes do lend�rio Distrito 13, enfim � poss�vel organizar uma resist�ncia. Come�ou a revolu��o. A coragem de Katniss nos jogos fez nascer a esperan�a em um pa�s disposto a fazer de tudo para se livrar da opress�o. E agora, contra a pr�pria vontade, ela precisa assumir seu lugar como s�mbolo da causa rebelde. Ela precisa virar o Tordo. O sucesso da revolu��o depender� de Katniss aceitar ou n�o essa responsabilidade. Ser� que vale a pena colocar sua fam�lia em risco novamente? Ser� que as vidas de Peeta e Gale ser�o os tributos exigidos nessa nova guerra? Acompanhe Katniss at� o fim do thriller, numa jornada ao lado mais obscuro da alma humana, em uma luta contra a opress�o e a favor da esperan�a.'
	
go

Exec upsertBook
    @uuid = '1259ef59-7310-4330-b966-ea65c772dc74',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857980062',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go

Exec upsertBook
    @uuid = '63a4c060-0f4e-11eb-adc1-0242ac120002',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Jogos Vorazes - A cantiga dos p�ssaros e das serpentes ',
	@author = 'Suzanne Collins',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '576',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 23,
	@genre = 'Fic��o Cient�fica',
	@price = 39.90,
	@isbn = '6556670006',
	@synopsis = 'UMA HIST�RIA DA S�RIE JOGOS VORAZES. AMBI��O O ALIMENTAR�. COMPETI��O O CONDUZIR�. MAS O PODER TEM O SEU PRE�O. � a manh� do dia da colheita que iniciar� a d�cima edi��o dos Jogos Vorazes. Na Capital, o jovem de dezoito anos Coriolanus Snow se prepara para sua oportunidade de gl�ria como um mentor dos Jogos. A outrora importante casa Snow passa por tempos dif�ceis e o destino dela depende da pequena chance de Coriolanus ser capaz de encantar, enganar e manipular seus colegas estudantes para conseguir mentorar o tributo vencedor. A sorte n�o est� a favor dele. A ele foi dada a tarefa humilhante de mentorar a garota tributo do Distrito 12, o pior dos piores. Os destinos dos dois est�o agora interligados � toda escolha que Coriolanus fizer pode significar sucesso ou fracasso, triunfo ou ru�na. Na arena, a batalha ser� mortal. Fora da arena, Coriolanus come�a a se apegar a j� condenada garota tributo... e dever� pesar a necessidade de seguir as regras e o desejo de sobreviver custe o que custar.'

go
Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857981069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go






Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '215981069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '311981069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '003381069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go





Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '126981069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '854981069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857902069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go





Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '850081069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '007781069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857781069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '157981069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '989f2091-8f62-4c3e-a5f3-8b45bd9fe06a',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '827980469',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = 'c7a09862-12db-4493-af8a-11c0c914b87d',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857172569',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '7378e600-8241-4f52-ac71-a4d622290d3d',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857984569',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '1708c2c7-9224-48b0-a461-2876a7f70ef2',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857231069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '994b5c98-99d3-48da-bbef-05cda85898af',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 0,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857989069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = 'dafca49f-6d88-40a6-a54b-5cbf00394feb',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857981769',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '99a513fe-bc77-4ee7-9172-68c6f0956d94',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 0,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857981069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '6e946927-efbd-43b1-a6eb-81330491ad1c',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 0,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '855981069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '989f2091-ecc06781-7bee-4aa4-9595-66d30b44be35',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857981039',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go




Exec upsertBook
    @uuid = '3d481b0d-27a9-4756-b102-330ad1b51897',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '852981069',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go



Exec upsertBook
    @uuid = '54e01556-2570-4d85-95ed-57b83b8cb868',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Lorem Ipsum is not simply',
	@author = 'Lorem Ipsum ',
	@cover = 'https://images-na.ssl-images-amazon.com/images/I/319PdrVPsJL._SX311_BO1,204,203,200_.jpg',
	@pages = '416',
	@publishing_company = 'popular belief',
	@quantity = 16,
	@genre = 'romance',
	@price = 34.90,
	@isbn = '857981079',
	@synopsis = 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.'
go



-- 1 carrinho sendo comprado --

Exec upsertCart
	@uuid = '465eb438-0f4f-11eb-adc1-0242ac120002',
	@delivery_addresses_uuid = '33e0469e-0f4c-11eb-adc1-0242ac120002',
	@bought_in = '',
	@status_carts = 'aguardando'

go
-- 1 Endere�o --

Exec  upsertAddress

	@uuid ='33e0469e-0f4c-11eb-adc1-0242ac120002',
	@reader_people_uuid = '182ba040-0f49-11eb-adc1-0242ac120002',
	@number ='862',
	@cep = '15105000',
	@address = '7 de setembro',
	@neighborhood = 'Centro',
	@city = 'Potirendaba',
	@complement = 'Casa',
	@state_address = 'SP'

go
-- 1 item de carrinho --

Exec upsertCart
	@uuid = '465eb438-0f4f-11eb-adc1-0242ac120002',
	@delivery_addresses_uuid = '33e0469e-0f4c-11eb-adc1-0242ac120002',
	@bought_in = '2020-11-18T20:15:14',
	@status_carts = 'finalizado'
go


Exec upsertCart
	@uuid = '465eb438-0f4f-11eb-at52-0242ac120002',
	@delivery_addresses_uuid = '33e0469e-0f4c-11eb-adc1-0242ac120002',
	@bought_in = '2020-12-20T20:15:14',
	@status_carts = 'finalizado'
go


Exec upsertItemCart
	@cart_uuid = '465eb438-0f4f-11eb-adc1-0242ac120002',
	@book_uuid = '54e01556-2570-4d85-95ed-57b83b8cb868',
	@quantity = 2,
	@price = 27.00
go



Exec upsertItemCart
	@cart_uuid = '465eb438-0f4f-11eb-adc1-0242ac120002',
	@book_uuid = '3d481b0d-27a9-4756-b102-330ad1b51897',
	@quantity = 2,
	@price = 15.00
go


Exec upsertItemCart
	@cart_uuid = '465eb438-0f4f-11eb-adc1-0242ac120002',
	@book_uuid = '6e946927-efbd-43b1-a6eb-81330491ad1c',
	@quantity = 4,
	@price = 50.00
go




Exec upsertItemCart
	@cart_uuid = '465eb438-0f4f-11eb-at52-0242ac120002',
	@book_uuid = '3d481b0d-27a9-4756-b102-330ad1b51897',
	@quantity = 1,
	@price = 15.00
go


Exec upsertItemCart
	@cart_uuid = '465eb438-0f4f-11eb-at52-0242ac120002',
	@book_uuid = '6e946927-efbd-43b1-a6eb-81330491ad1c',
	@quantity = 7,
	@price = 25.00
go

-- 1 leitor --
Exec upsertReader  
	@uuid='182ba040-0f49-11eb-adc1-0242ac120000',
	@people_name = 'Willian Liduenha',
	@cpf ='32431123033',
	@birth ='2001-10-27',
	@people_login = 'will',
	@people_password = '123',
	@photo = 'https://scontent.fsjp2-1.fna.fbcdn.net/v/t1.0-9/116883432_1532413303604225_1545760922914374817_o.jpg?_nc_cat=102&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeH9EMdLAeJdbD3dU5OTLUdrlQm2JZ34IsmVCbYlnfgiydZdzBYINbUF1-yI4XHt28-ih4l588RXN8iQqsEqtlCI&_nc_ohc=tXdPZhVIJ-MAX9A3Wl-&_nc_ht=scontent.fsjp2-1.fna&oh=6f6a21681845a66ce5737dd155ef898e&oe=5FF167FB',
	@telephone ='135674289',
	@email ='Will@hotmail.com',
	@about = 'Ola, meu nome é Willian!'

go

-- 1 funcionario administrador --
Exec upsertEmployee
    @uuid = 'd8de96c6-0f59-11eb-adc1-0242ac120000',
    @people_name = 'Lucas Otaviani',
    @cpf = '45232802881',
    @birth = '2001-12-12',
	@photo = 'https://scontent.fsjp2-1.fna.fbcdn.net/v/t1.0-9/62174015_2356505524568578_8373343948223021056_o.jpg?_nc_cat=101&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGx4O0m9QsS7Nu3aaPU5kNwC2KdngnG7kwLYp2eCcbuTEhGIZc2gh1vNu2eH7RnwjFEjtPlpvZ_UnxaG0o6TZxi&_nc_ohc=-1UXOnt3XxUAX-og9ag&_nc_ht=scontent.fsjp2-1.fna&oh=78e4ac30b280db699b82275e0cffe865&oe=5FEED978',
    @people_login = 'otaviani',
    @people_password = '123',
    @telephone = '987521443',
    @email = 'otaviani@hotmail.com',
	@about= 'Sou o Lucas, mas pode me chamar de Otaviani :)',

    @employee_type = 'administrador',
    @employee_status = 'ativo'

go


-- 3 funcion�rios comuns --
Exec upsertEmployee
    @uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200000',
    @people_name = 'Aline Gimenez',
    @cpf = '36487143099',
    @birth = '2001-02-24',
	@photo = 'https://scontent.fsjp2-1.fna.fbcdn.net/v/t1.0-9/24991244_961397557356171_2550687741204049218_n.jpg?_nc_cat=105&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeEKupYwwhhaL_dTwdWfj-ilarnhDy5o5-RqueEPLmjn5ImniZAR3VfPzjsRUpBcf3YXdqfYe0UHasvhzxGv3_eD&_nc_ohc=uQxFzBFWZCkAX82ZvF5&_nc_ht=scontent.fsjp2-1.fna&oh=90b55de7acf04bf669bda58700a7512f&oe=5FEF6199',
    @people_login = 'aline',
    @people_password = '123',
    @telephone = '997634210',
    @email = 'AlineGimenez@hotmail.com',
	@about= 'Oi oi, meu nome é Aline, meus tipos favoritos de leitura são de suspense e mistério! Agatha Christie faz parte do meu vocabulário, então cuidado comigo!',
	
    @employee_type = 'colaborador',
    @employee_status = 'ativo'

go

Exec upsertEmployee
    @uuid = 'd63c860c-0f4a-11eb-adc1-0242ac120000',
    @people_name = 'Davi Massaru',
    @cpf = '85565346402',
    @birth = '2000-12-16',
	@photo = 'https://scontent.fsjp2-1.fna.fbcdn.net/v/t1.0-9/106496146_2792093874262521_5564406573914838681_n.jpg?_nc_cat=107&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeFxC-CsDcRaev5hg_OdzR-wkPNEU8XHqvaQ80RTxceq9tiY2tutk4uerAsm8sAW2-idpRyW0pIj5_YD_h8xYLP0&_nc_ohc=TbTiVInqUFgAX_k4iRC&_nc_ht=scontent.fsjp2-1.fna&oh=af3d7a8a3c9d661a732758fd8b47edf5&oe=5FEE4284',
    @people_login = 'davi',
    @people_password = '123',
    @telephone = '9912567384',
    @email = 'davi@hotmail.com',
	@about= 'Se necessário, contate o suporte... Ou seja, EU!',

    @employee_type = 'colaborador',
    @employee_status = 'demitido'

go

Exec upsertEmployee
    @uuid = '33741272-0f4b-11eb-adc1-0242ac120000',
    @people_name = 'Noah Teixeira',
    @cpf = '31144144062',
    @birth = '2001-03-02',
	@photo = 'https://thispersondoesnotexist.com/image',
    @people_login = 'noah',
    @people_password = '321',
    @telephone = '987563712',
    @email = 'noah@hotmail.com',
	@about= 'Livros são minha paixão.',

    @employee_type = 'colaborador',
    @employee_status = 'suspenso'

go

-- 1 Endereco -- 
Exec upsertAddress
	@uuid = '33e0469e-0f4c-11eb-adc1-0242ac120000',
	@reader_people_uuid = '182ba040-0f49-11eb-adc1-0242ac120000',
	@number = '874',
	@cep = '49030-650',
	@address = 'Rua Presbtero Alosio Santos',
	@neighborhood = 'Farolndia',
	@city = 'Aracaju',
	@complement = 'Casa',
	@state_address = 'SE'

go

-- 4 livros --
Exec upsertBook
    @uuid = '7a6264d4-0f4d-11eb-adc1-0242ac125896',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200000',
	@name = 'Jogos Vorazes',
	@author = 'Suzanne Collins',
	@cover = 'http://skoob.s3.amazonaws.com/livros/106468/JOGOS_VORAZES_1274398229P.jpg',
	@pages = '400',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 20,
	@genre = 'Ficcao Cientifica',
	@price = 27.00,
	@isbn = '9788579800245',
	@synopsis = 'Ap�s o fim da Am�rica do Norte, uma nova na��o chamada Panem surge. Formada por doze distritos, � comandada com m�o de ferro pela Capital. Uma das formas com que demonstram seu poder sobre o resto do carente pa�s � com Jogos Vorazes, uma competi��o anual transmitida ao vivo pela televis�o, em que um garoto e uma garota de doze a dezoito anos de cada distrito s�o selecionados e obrigados a lutar at� a morte! Para evitar que sua irm� seja a mais nova v�tima do programa, Katniss se oferece para participar em seu lugar. Vinda do empobrecido distrito 12, ela sabe como sobreviver em um ambiente hostil. Peeta, um garoto que ajudou sua fam�lia no passado, tamb�m foi selecionado. Caso ven�a, ter� fama e fortuna. Se perder, morre. Mas para ganhar a competi��o, ser� preciso muito mais do que habilidade. At� onde Katniss estar� disposta a ir para ser vitoriosa nos Jogos Vorazes?'

go

Exec upsertBook
    @uuid = '7a6264d4-0f4d-11eb-adc1-0242ac120000',
	@employee_people_uuid = 'd63c860c-0f4a-11eb-adc1-0242ac120000',
	@name = 'Jogos Vorazes - Em chamas',
	@author = 'Suzanne Collins',
	@cover = 'http://skoob.s3.amazonaws.com/livros/144526/EM_CHAMAS_1302531463P.jpg',
	@pages = '416',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 16,
	@genre = 'Ficcao Cientifica',
	@price = 34.90,
	@isbn = '8579800667',
	@synopsis = 'Depois do improv�vel e inusitado resultado dos �ltimos Jogos Vorazes, algo parece ter mudado para sempre em Panem. Aqui e ali, dist�rbios e agita��es nos distritos d�o sinais de que uma revolta � iminente. Katniss e Peeta, representantes do paup�rrimo Distrito 12, n�o apenas venceram os Jogos, mas ridicularizaram o governo e conseguiram fazer todos � incluindo o pr�prio Peeta � acreditarem que s�o um casal apaixonado. A confus�o na cabe�a de Katniss n�o � menos do que a das ruas. Em meio ao turbilh�o, ela pensa cada vez mais em seu melhor amigo, o jovem ca�ador Gale, mas � obrigada a fingir que o romance com Peeta � real. J� o governo parece especialmente preocupado com a influ�ncia que os dois adolescentes vitoriosos � transformados em verdadeiros �dolos nacionais � podem ter na popula��o. Por isso, existem planos especiais para mant�-los sob controle, mesmo que isso signifique for��-los a lutar novamente.'

go


Exec upsertBook
    @uuid = 'e5184bc2-0f4d-11eb-adc1-0242ac120000',
	@employee_people_uuid = '33741272-0f4b-11eb-adc1-0242ac120000',
	@name = 'Jogos Vorazes - A esperan�a',
	@author = 'Suzanne Collins',
	@cover = 'http://skoob.s3.amazonaws.com/livros/175390/A_ESPERANCA_1316052958P.jpg',
	@pages = '424',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 0,
	@genre = 'Fic��o Cient�fica',
	@price = 15.90,
	@isbn = '8579840862',
	@synopsis = 'Depois de sobreviver duas vezes � crueldade de uma arena projetada para destru�-la, Katniss acreditava que n�o precisaria mais de lutar. Mas as regras do jogo mudaram: com a chegada dos rebeldes do lend�rio Distrito 13, enfim � poss�vel organizar uma resist�ncia. Come�ou a revolu��o. A coragem de Katniss nos jogos fez nascer a esperan�a em um pa�s disposto a fazer de tudo para se livrar da opress�o. E agora, contra a pr�pria vontade, ela precisa assumir seu lugar como s�mbolo da causa rebelde. Ela precisa virar o Tordo. O sucesso da revolu��o depender� de Katniss aceitar ou n�o essa responsabilidade. Ser� que vale a pena colocar sua fam�lia em risco novamente? Ser� que as vidas de Peeta e Gale ser�o os tributos exigidos nessa nova guerra? Acompanhe Katniss at� o fim do thriller, numa jornada ao lado mais obscuro da alma humana, em uma luta contra a opress�o e a favor da esperan�a.'
	
go

Exec upsertBook
    @uuid = '1259ef59-7310-4330-b966-ea65c772dc74',
	@employee_people_uuid = 'd63c860c-0f4a-11eb-adc1-0242ac120000',
	@name = 'Harry Potter e a Pedra Filosofal',
	@author = 'J. K. Rowling',
	@cover = 'http://skoob.s3.amazonaws.com/livros/108/HARRY_POTTER_E_A_PEDRA_FILOSOFAL_1348758176P.jpg',
	@pages = '416',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 16,
	@genre = 'Aventura',
	@price = 34.90,
	@isbn = '857950072',
	@synopsis = 'Harry Potter é um garoto cujos pais, feiticeiros, foram assassinados por um poderosíssimo bruxo quando ele ainda era um bebê. Ele foi levado, então, para a casa dos tios que nada tinham a ver com o sobrenatural. Pelo contrário. Até os 10 anos, Harry foi uma espécie de gata borralheira: maltratado pelos tios, herdou roupas velhas do primo gorducho, teve óculos remendados e foi tratado como um estorvo. No dia de seu aniversário de 11 anos, ele parece deslizar por um buraco sem fundo, como o de Alice no País das Maravilhas, que o conduz a um mundo mágico. Descobre sua verdadeira história e seu destino: ser um aprendiz de feiticeiro até o dia em que terá que enfrentar a pior força do mal, o homem que assassinou seus pais. O menino de olhos verde, magricela e desengonçado, tão habituado à rejeição, descobre, também, que é um herói no universo dos magos. Potter fica sabendo que é a única pessoa a ter sobrevivido a um ataque do tal bruxo do mal e essa é a causa da marca em forma de raio que ele carrega na testa. Ele não é um garoto qualquer, ele sequer é um feiticeiro qualquer; ele é Harry Potter, símbolo de poder, resistência e um líder natural entre os sobrenaturais. A fábula, recheada de fantasmas, paredes que falam, caldeirões, sapos, unicórnios, dragões e gigantes, não é, entretanto, apenas um passatempo. Harry Potter conduz a discussões metafísicas, aborda o eterno confronto entre o bem e o mal, evidencia algumas mazelas da sociedade, como o preconceito, a divisão de classes através do dinheiro e do berço, a inveja, o egoísmo, a competitividade exacerbada, a busca pelo ideal – a necessidade de aprender, nem que seja à força, que a vida é feita de derrotas e vitórias e que isso é importante para a formação básica de um adulto.'
go

Exec upsertBook
    @uuid = '1259ef59-7310-4330-b966-ea65c772dc75',
	@employee_people_uuid = 'd63c860c-0f4a-11eb-adc1-0242ac120000',
	@name = 'Harry Potter e a Câmara Secreta',
	@author = 'J. K. Rowling',
	@cover = 'http://skoob.s3.amazonaws.com/livros/357/HARRY_POTTER_E_A_CAMARA_SECRETA_1343592468P.jpg',
	@pages = '526',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 12,
	@genre = 'Aventura',
	@price = 40.00,
	@isbn = '857950162',
	@synopsis = 'Câmara Secreta, onde ele vai reencontrar todos os pequenos heróis e amigos do livro anterior. Mas isto não será para sempre. J. K. Rowling, a autora da saga de Harry Potter, já avisou que até o sétimo livro da série, que promete ser o último, alguns personagens do bem vão morrer. A trama de Harry Potter e a Câmara Secreta começa com o pequeno feiticeiro passando as férias na casa de seus tios trouxas (não-bruxos) e sendo, como sempre, muito maltratado. Seu aniversário de 12 anos é o pior de todos: ninguém o cumprimenta, não ganha nenhum presente, nada. O garoto, órfão de pai e mãe, chega a cantar Parabéns pra você baixinho como se quisesse, ele próprio, provar que está vivo. Para piorar, os tios o prendem num quarto cercado de grades com direito a apenas uma refeição por dia – que ele divide com sua coruja, igualmente encarcerada numa gaiola. De repente, aparece um carro voador com amigos feiticeiros que livram Harry Potter dessa amargura. Essa é apenas a primeira cena em que Joanne brinca com situações-limite. Todo o livro é permeado de quase-desgraças e é, por isso mesmo, quase impossível parar de ler. A empreitada, dessa vez, consiste em localizar uma câmara secreta e liquidar o monstro que está atacando estudantes do colégio Hogwarts, no qual os pequenos feiticeiros estudam magia e se divertem aprendendo, por exemplo, a transformar as plantas usando adubo de dragão.Para Harry, garoto sem família e rejeitado pelos tios, Hogwarts é tudo. Portanto, quando colegas, e até professores, começam a desconfiar que ele tem alguma participação nas tragédias que estão acontecendo no colégio, a situação fica mais complicada. Até Hermione, amiga querida de Potter, é atacada pelo monstro e se transforma numa estátua. Só resta ao nosso herói tentar resolver o mistério por conta própria. Mais uma vez, ele enfrenta o terrível bruxo das trevas e… o final é surpreendente e muito divertido.'
go

Exec upsertBook
    @uuid = '1259ef59-7310-4330-b966-ea65c772dc76',
	@employee_people_uuid = 'd63c860c-0f4a-11eb-adc1-0242ac120000',
	@name = 'Harry Potter e o Prisioneiro de Azkaban',
	@author = 'J. K. Rowling',
	@cover = 'http://skoob.s3.amazonaws.com/livros/358/HARRY_POTTER_E_O_PRISIONEIRO_DE_AZKABAN_1343592651P.jpg',
	@pages = '487',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 16,
	@genre = 'Aventura',
	@price = 34.90,
	@isbn = '857980362',
	@synopsis = 'Expresso a vapor que o levará de volta à escola de bruxaria. Mais uma vez suas férias na rua dos Alfeneiros, 4, foi triste e solitária. Tio Válter Dursley estava especialmente irritado com ele, porque seu amigo Rony Weasley tinha lhe telefonado. E ele não aceitava qualquer ligação de Harry com o mundo dos mágicos dentro de sua casa. A situação piorou ainda mais com a chegada de tia Guida, irmã de Válter. Harry já estava acostumado a ser humilhado pelos Dursley, mas quando tia Guida passou a ofender os pais de Harry, mortos pelo bruxo Voldemort, ele não agüentou e transformou-a num imenso balão. Irritado, fugiu da casa dos tios, indo se abrigar no Beco Diagonal. Lá ele reencontra Rony e Hermione, seus melhores amigos em Hogwarts e, para sua surpresa, é procurado pelo próprio Ministro da Magia. Sem que Harry saiba, o ministro está preocupado com o garoto, pois fugiu da prisão de Azkaban o perigoso bruxo Sirius Black, que teria assassinado treze pessoas com um único feitiço e traído os pais de Harry, entregando-os a Voldemort. Sob forte escolta, o garoto é levado para Hogwarts. Na escola as dificuldades são as de sempre: Severo Snape, o professor de Poções, o trata cada vez pior, enquanto ele tem de se esforçar nos treinos de quadribol, e levar Grifinória à vitória do campeonato. Para piorar a situação, os terríveis guardas de Azkaban, conhecidos por dementadores, estão de guarda nos portões da escola, caso Sirius Black tente algo contra Harry. Por fim, Harry tem de enfrentar seu inimigo para salvar Rony e obrigado a escolher entre matar ou não aquele que traiu seus pais. Com muita ação, humor e magia, ”Harry Potter e o prisioneiro de Azkaban” traz de volta o gigante atrapalhado Rúbeo Hagrid, o sábio diretor Alvo Dumbledore, a exigente professora de transformação Minerva MacGonagall e o novo mestre Lupin, que guarda grandes surpresas para Harry.'
go

Exec upsertBook
    @uuid = '1259ef59-7310-4330-b966-ea65c772dc77',
	@employee_people_uuid = 'd63c860c-0f4a-11eb-adc1-0242ac120000',
	@name = 'Harry Potter e o Cálice de Fogo',
	@author = 'J. K. Rowling',
	@cover = 'http://skoob.s3.amazonaws.com/livros/108/HARRY_POTTER_E_A_PEDRA_FILOSOFAL_1348758176P.jpg',
	@pages = '416',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 0,
	@genre = 'Aventura',
	@price = 34.90,
	@isbn = '857983462',
	@synopsis = '
As férias de verão vão se arrastando e Harry pottar mal pode esperar pelo inicio do ano letivo. é o seu quarto ano na Escola de Magia e Bruxaria de Hogwarts, e há feitiços a serem preparadas e aulas de Adivinhação, entre outras, a serem assistidas. Harry anseia por tudo isso. Porém, muitos acontecimentos surpreendentes já estão em marcha…'
go

Exec upsertBook
    @uuid = '1259ef59-7310-4330-b966-ea65c772dc78',
	@employee_people_uuid = 'd63c860c-0f4a-11eb-adc1-0242ac120000',
	@name = 'Harry Potter e a Ordem da Fênix',
	@author = 'J. K. Rowling',
	@cover = 'http://skoob.s3.amazonaws.com/livros/359/HARRY_POTTER_E_A_ORDEM_DA_FENIX_1343593292P.jpg',
	@pages = '316',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 16,
	@genre = 'Aventura',
	@price = 34.90,
	@isbn = '857920062',
	@synopsis = 'Harry Potter chegou à adolescência. E, junto com as transformações desta época tão importante, vive seus dias mais sombrios na escola de magia de Hogwarts. Aos 15 anos, continua sofrendo a rejeição dos Dursdley, sua estranhíssima família no mundo dos trouxas, ou seja, todos os que não são bruxos. Também continua contando com Rony Weasley e Hermione Granger, seus melhores amigos em Hogwarts, para levar adiante suas investigações e aventuras. Mas o bruxinho começa a sentir e descobrir coisas novas, como o primeiro amor e a sexualidade. Nos volumes anteriores, J. K. Rowling mostrou como Harry foi transformado em celebridade no mundo da magia por ter derrotado, ainda bebê, Voldemort, o todo-poderoso bruxo das trevas que assassinou seus pais. Neste quinto livro da saga, o protagonista, numa crise típica da adolescência, tem ataques de mau humor com a perseguição da imprensa, que o segue por todos os lugares e chega a inventar declarações que nunca deu. Harry vai enfrentar as investidas de Voldemort sem a proteção de Dumbledore, já que o diretor de Hogwarts é afastado da escola. E vai ser sem a ajuda de seu protetor que o jovem herói enfrentará descobertas sobre a personalidade controversa de seu pai, Tiago Potter, e a já anunciada morte de alguém muito próximo. O desaparecimento de um dos personagens centrais da trama é um dos trunfos de A Ordem da Fênix que, com isto, transforma-se no livro mais dramático da série até agora.'
go

Exec upsertBook
    @uuid = '1259ef59-7310-4330-b966-ea65c772dc84',
	@employee_people_uuid = 'd63c860c-0f4a-11eb-adc1-0242ac120000',
	@name = 'Harry Potter e o Enigma do Príncipe',
	@author = 'J. K. Rowling',
	@cover = 'http://skoob.s3.amazonaws.com/livros/562/HARRY_POTTER_E_O_ENIGMA_DO_PRINCIPE_1343593517P.jpg',
	@pages = '416',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 2,
	@genre = 'Aventura',
	@price = 78.90,
	@isbn = '857982262',
	@synopsis = '“Harry Potter e o Enigma do Príncipe” dá continuidade à saga do jovem bruxo Harry Potter a partir do ponto onde o livro anterior parou, o momento em que fica provado que o poder de Voldemort e dos Comensais da Morte, seus seguidores, cresce mais a cada dia, em meio à batalha entre o bem e o mal. A onda de terror provocada pelo Lorde das Trevas estaria afetando, até mesmo, o mundo dos trouxas (não-bruxos), e sendo agravada pela ação dos dementadores, criaturas mágicas aterrorizantes que “sugam” a esperança e a felicidade das pessoas. Harry, que acabou de completar 16 anos, parte rumo ao sexto ano na Escola de Magia e Bruxaria de Hogwarts, animado e, ao mesmo tempo, apreensivo com a perspectiva de ter aulas particulares com o professor Dumbledore, o diretor da escola e o bruxo mais respeitado em toda comunidade mágica. Harry, longe de ser aquele menino magricela que vivia no quarto debaixo da escada na casa dos tios trouxas, é um dos principais nomes entre aqueles que lutam contra Voldemort, e se vê cada vez mais isolado à medida em que os rumores de que ele é O Eleito, o único capaz de derrotar o Lorde das Trevas, se espalham pelo mundo bruxo. Dois atentados contra a vida de estudantes, a certeza de Harry quanto ao envolvimento de Draco Malfoy com os Comensais da Morte e o comportamento de Snape, suspeito como sempre, adicionam ainda mais tensão ao já inquietante período. Apesar de tudo isso, ele e os amigos são adolescentes típicos: dividem tarefas escolares e dormitórios bagunçados, correm das aulas para os treinos de quadribol, e namoram. Rony e Hermione, os melhores amigos de Harry, se dão conta (finalmente!) da atração que sentem um pelo outro; Harry e Gina, a irmã mais nova de Rony, também. Muitas peças do intricado quebra-cabeça criado por J. K. Rowling começam a se encaixar, à medida em que a escritora começa a preparar Harry (e os leitores) para o desfecho da série. Informações são reveladas por meio do uso da Penseira, um objeto que permite compartilhar memórias, utilizado por Harry e o professor Dumbledore para viajar no tempo, e por diferentes lugares, em busca de explicações sobre o passado de Voldemort. O final de “Harry Potter e o Enigma do Príncipe” é de parar o coração.'
go

Exec upsertBook
    @uuid = '1259ef59-7310-4330-b966-ea65c872dc74',
	@employee_people_uuid = 'd63c860c-0f4a-11eb-adc1-0242ac120000',
	@name = 'Harry Potter e as Relíquias da Morte',
	@author = 'J. K. Rowling',
	@cover = 'http://skoob.s3.amazonaws.com/livros/1200/HARRY_POTTER_E_AS_RELIQUIAS_DA_MORTE_1343593859P.jpg',
	@pages = '786',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 16,
	@genre = 'Aventura',
	@price = 34.90,
	@isbn = '857980062',
	@synopsis = '
Em Harry Potter E As Relíquias Da Morte, o encontro inevitável com Lord Voldemort não pode mais ser adiado. Harry, no entanto, precisa ganhar tempo para encontrar as Horcruxes que ainda estão faltando. E, pelo caminho, descobrir o que são afinal as Relíquias da Morte e como ele pode usá-las contra o Lorde das Trevas. Seguindo as poucas pistas deixadas por Dumbledore, Harry conta apenas com a ajuda dos leais amigos Rony e Hermione. Juntos, eles percorrem lugares nunca visitados, descobrem histórias nebulosas sobre pessoas queridas e acabam por desvendar mistérios que os incomodavam há muito tempo. Enquanto Harry, Rony e Hermione vagam por diferentes lugares em busca de pistas, J. K. Rowling vai revelando aspectos até então desconhecidos sobre os principais personagens. Em sua última e derradeira aventura, Harry não é exposto apenas a batalhas. Ele precisa superar traições, surpresas e, mais do que nunca, aprender a lidar com os próprios sentimentos. Como em todos os livros da série, o amor e a amizade são elementos-chave para a trama. Em Harry Potter E As Relíquias Da Morte, J. K. Rowling leva o leitor por uma trilha de suspense, com sustos ininterruptos até a última página, quando entrega, por completo, toda a verdade e conclui os passos de herói de Harry Potter na maior saga bruxa de todos os tempos.'
go


-- 1 carrinho sendo comprado --

Exec upsertCart
	@uuid = '465eb438-0f4f-11eb-adc1-0242ac120312',
	@delivery_addresses_uuid = '33e0469e-0f4c-11eb-adc1-0242ac120000',
	@bought_in = '2020-10-18T20:15:14',
	@status_carts = 'finalizado'

go
-- 1 Endere�o --

Exec  upsertAddress

	@uuid ='33e0469e-0f4c-11eb-adc1-0242ac120234',
	@reader_people_uuid = '182ba040-0f49-11eb-adc1-0242ac120000',
	@number ='862',
	@cep = '15105000',
	@address = '7 de setembro',
	@neighborhood = 'Centro',
	@city = 'Potirendaba',
	@complement = 'Casa',
	@state_address = 'SP'

go
-- 1 item de carrinho --

Exec upsertCart
	@uuid = '465eb438-0f4f-11eb-adc1-0242ac12034',
	@delivery_addresses_uuid = '33e0469e-0f4c-11eb-adc1-0242ac120234',
	@bought_in = '2020-11-18T20:15:14',
	@status_carts = 'finalizado'
go

Exec upsertItemCart
	@cart_uuid = '465eb438-0f4f-11eb-adc1-0242ac12034',
	@book_uuid = '1259ef59-7310-4330-b966-ea65c872dc74',
	@quantity = 2,
	@price = 34.90
go



Exec upsertItemCart
	@cart_uuid = '465eb438-0f4f-11eb-adc1-0242ac12034',
	@book_uuid = '1259ef59-7310-4330-b966-ea65c772dc84',
	@quantity = 2,
	@price = 78.90
go


Exec upsertItemCart
	@cart_uuid = '465eb438-0f4f-11eb-adc1-0242ac120312',
	@book_uuid = '1259ef59-7310-4330-b966-ea65c772dc84',
	@quantity = 8,
	@price = 78.90
go
