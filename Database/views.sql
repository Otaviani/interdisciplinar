use library_ecommerce
go

/* get readers information */
create view vwReader
as
    select 
		people.uuid,
		people.people_login as login,
		people.people_password as password,
		people.people_name As name,
		people.cpf,
		people.birth,
		people.photo As photo,
		telephone,
		email,
		readers.about
	from readers 
	inner join people ON readers.people_uuid = people.uuid;
go
--SELECT * FROM vwReader

create view vwEmployees
as
    select 
		people.uuid,
		people.people_login As login,
		people.people_password As password,
		people.people_name As name,
		people.cpf,
		people.birth,
		people.photo As photo,
		readers.about,
		telephone,
		email,
		employee_status,
		employee_type
	from employees 
	inner join people ON employees.people_uuid = people.uuid
	inner join readers ON employees.people_uuid = readers.people_uuid;
go
--SELECT * FROM vwEmployees

create view vwReaderAddresses
as
    Select 
	delivery_addresses.uuid AS addresses_uuid ,
	delivery_addresses.reader_people_uuid AS people_uuid,
	delivery_addresses.number,
	delivery_addresses.cep,
	delivery_addresses.address,
	delivery_addresses.neighborhood,
	delivery_addresses.city,
	delivery_addresses.complement,
	delivery_addresses.state_address,
	delivery_addresses.created_at ,
	delivery_addresses.updated_at ,
	delivery_addresses.deleted_at
	From delivery_addresses 
	inner join readers ON delivery_addresses.reader_people_uuid = readers.people_uuid
go
--SELECT * FROM vwReaderAddresses

create view vwReaderCarts
as
    Select 
		delivery_addresses.reader_people_uuid AS people_uuid,
		carts.uuid,
		carts.delivery_addresses_uuid,
		carts.bought_in,
		carts.status_carts
	From readers 
		inner join delivery_addresses ON delivery_addresses.reader_people_uuid = readers.people_uuid
		inner join carts ON delivery_addresses.uuid = carts.delivery_addresses_uuid
go
--SELECT * FROM vwReaderCarts

create view vwLast5BookReleases
as 
	SELECT TOP(5)
    	books.uuid,
		books.name,
    	books.employee_people_uuid,
		books.author,
		books.pages,
		books.publishing_company,
		books.quantity,
		books.genre,
		books.price,
		books.isbn ,
		books.cover,
		books.synopsis,
		books.created_at,
		books.updated_at,
		books.deleted_at
	From  books
	WHERE
		books.deleted_at is NULL
	ORDER BY books.created_at DESC
go
--SELECT * FROM vwLast5BookReleases

create view vwBooksWithStock
as
	SELECT
    	books.uuid,
		books.name,
    	books.employee_people_uuid,
		books.author,
		books.pages,
		books.publishing_company,
		books.quantity,
		books.genre,
		books.price,
		books.isbn ,
		books.cover,
		books.synopsis,
		books.created_at,
		books.updated_at,
		books.deleted_at
		From  books
	WHERE
    	books.quantity > 0 AND
		books.deleted_at is NULL
go
-- SELECT * FROM vwBooksWithStock

create view vwBooksWithoutStock
as
	SELECT
    	books.uuid,
		books.name,
    	books.employee_people_uuid,
		books.publishing_company,
		books.author,
		books.pages,
		books.quantity,
		books.genre,
		books.price,
		books.isbn ,
		books.cover,
		books.synopsis,
		books.created_at,
		books.updated_at,
		books.deleted_at
		From  books
	WHERE
    	books.quantity <= 0 AND
		books.deleted_at is NULL
go
-- SELECT * FROM vwBooksWithoutStock

create view vwGetAllBooks
as
	SELECT
			books.uuid,
			books.name,
			books.employee_people_uuid,
			books.author,
			books.pages,
			books.publishing_company,
			books.quantity,
			books.genre,
			books.price,
			books.isbn ,
			books.cover,
			books.synopsis,
			books.created_at,
			books.updated_at,
			books.deleted_at
		From  books
go
--SELECT * FROM vwGetAllBooks

create view vwReportSalesBooks
as
	SELECT 
		carts.bought_in,
		books.uuid uuid,
		books.name name,
		items_cart.quantity sales,
		items_cart.quantity * items_cart.price gain
	FROM
		books
		INNER JOIN items_cart ON items_cart.book_uuid = books.uuid
		INNER JOIN carts ON items_cart.cart_uuid = carts.uuid

--SELECT * FROM vwReportSalesBooks
go

create view vwGenres
as
	SELECT 
		books.genre
	FROM
		books
	GROUP BY books.genre
Go
-- SELECT * FROM vwGenres