use library_ecommerce
go

/* upsert people*/
create procedure upsertPeople
(
	@uuid char(36),
	@people_name varchar(50),
	@cpf char(11),
	@birth date,
	@photo varchar(max),
	@people_login varchar(25),
	@people_password varchar(max)
)
as
begin
	IF EXISTS(select 1 from people where people.uuid = @uuid)
		begin

			UPDATE people
					SET 
						people_name = @people_name,
						cpf = @cpf,
						birth = @birth,
						photo = @photo,
						people_login  = @people_login,
						people_password  = @people_password,
						updated_at = GETDATE()
			WHERE people.uuid = @uuid;

		end
	ELSE  
		begin
			INSERT INTO people	( uuid,  people_name,  cpf,  birth,photo,  people_login,  people_password,created_at)
			VALUES				(@uuid, @people_name, @cpf, @birth,@photo, @people_login, @people_password,GETDATE());
		end
end
go

/* upsert reader */
create procedure upsertReader
(
	@uuid char(36),
	@people_name varchar(50),
	@cpf char(11),
	@birth date,
	@photo varchar(max),
	@people_login varchar(25),
	@people_password varchar(max),
	@telephone varchar(11),
	@email varchar(50),
	@about varchar(max)
)
as
begin
	EXEC upsertPeople 	@uuid=@uuid,	@people_name=@people_name,	@cpf=@cpf,	@birth=@birth, @photo=@photo,	@people_login=@people_login,	@people_password=@people_password
	IF EXISTS(select 1 from readers where readers.people_uuid = @uuid)
		begin
			UPDATE readers
					SET 
						telephone = @telephone,
						email = @email,
						about = @about,
						updated_at = GETDATE()
			WHERE readers.people_uuid = @uuid;
		end
	ELSE  
		begin
			INSERT INTO readers	( telephone,  email, about,  people_uuid, created_at)
			VALUES				(@telephone, @email, @about,	    @uuid, GETDATE() );
		end
end
go

-- EXEMPLO DE EXECUÇÃO INSERT--
/*Exec upsertReader  
	@uuid='182ba040-0f49-11eb-adc1-0242ac120002',
	@people_name = 'Gama',
	@cpf ='32431123030',
	@birth ='2001-10-27',
	@people_login = 'gama123',
	@people_password = '132',

	@telephone ='135674289',
	@email ='gama@hotmail.com',
	@about = ''

go*/


/* save employee */
create procedure upsertEmployee
(
	@uuid char(36),
	@people_name varchar(50),
	@cpf char(11),
	@birth date,
	@photo varchar(max),
	@people_login varchar(25),
	@people_password varchar(max),

	@telephone varchar(11),
	@email varchar(50),
	@about varchar(380),
	
	@employee_type varchar(15),
	@employee_status  varchar(10)
)
as
begin
	EXEC upsertReader @uuid=@uuid,@people_name=@people_name,@cpf=@cpf,@birth=@birth,@people_login=@people_login,@photo=@photo,	@people_password=@people_password,	@telephone=@telephone,	@email=@email, @about=@about

	IF EXISTS(select 1 from employees where employees.people_uuid = @uuid)
		begin
			UPDATE employees
					SET 
						employee_type = @employee_type,
						employee_status = @employee_status,
						updated_at = GETDATE()
			WHERE employees.people_uuid = @uuid;
		end
	ELSE  
		begin
			INSERT INTO employees (people_uuid,  employee_type,  employee_status, created_at)
			VALUES				  (      @uuid, @employee_type, @employee_status, GETDATE() );
		end
end
go

-- EXEMPLO DE EXECUÇÃO INSERT --
/*Exec upsertEmployee
    @uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
    @people_name = 'Keyse',
    @cpf = '36487143090',
    @birth = '2002-11-13',
    @people_login = 'key123',
    @people_password = '123',
    @telephone = '997634210',
    @email = 'key@hotmail.com',
	@about= 'Teste',
	
    @employee_type = 'colaborador',
    @employee_status = 'ativo'

go*/

-- EXEMPLO DE EXECUÇÃO UPDATE --
/*Exec upsertEmployee
    @uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
    @people_name = 'Keyse',
    @cpf = '36487143090',
    @birth = '2002-11-13',
    @people_login = 'key123',
    @people_password = '123',
    @telephone = '997634210',
    @email = 'key@hotmail.com',
	@about= 'Teste',
	
    @employee_type = 'colaborador',
    @employee_status = 'demitido'

go*/


/* upsert delivery_addresses*/
create procedure upsertAddress
(
	@uuid char(36),
	@reader_people_uuid char(36),
	@number varchar(10),
	@cep varchar(8),
	@address varchar(50),
	@neighborhood varchar(50),
	@city varchar(50),
	@complement varchar(50),
	@state_address char(2)
)
as
begin
		IF EXISTS(select 1 from delivery_addresses where delivery_addresses.uuid = @uuid)
		begin
			UPDATE delivery_addresses
					SET 

						uuid=@uuid,
						reader_people_uuid=@reader_people_uuid,
						number=@number,
						cep=@cep,
						address=@address,
						neighborhood=@neighborhood,
						city=@city,
						complement=@complement,
						state_address=@state_address,
						updated_at=GETDATE()

			WHERE delivery_addresses.uuid = @uuid;
		end
	ELSE  
		begin
			INSERT INTO delivery_addresses ( uuid,	reader_people_uuid,	number,	 cep,  address,	 neighborhood,  city,  complement,	state_address,created_at)
			VALUES						   (@uuid, @reader_people_uuid,@number, @cep, @address, @neighborhood, @city, @complement, @state_address, GETDATE());
		end
end
go
-- EXEMPLO DE EXECUÇÃO INSERT --
/*Exec upsertAddress
	@uuid = '33e0469e-0f4c-11eb-adc1-0242ac120002',
	@reader_people_uuid = '182ba040-0f49-11eb-adc1-0242ac120002',
	@number = '874',
	@cep = '49030-650',
	@address = 'Rua Presbítero Aloísio Santos',
	@neighborhood = 'Farolândia',
	@city = 'Aracaju',
	@complement = '',
	@state_address = 'SE'

go*/

-- EXEMPLO DE EXECUÇÃO UPDATE --
/*Exec upsertAddress
	@uuid = '33e0469e-0f4c-11eb-adc1-0242ac120002',
	@reader_people_uuid = '182ba040-0f49-11eb-adc1-0242ac120002',
	@number = '999',
	@cep = '49030-650',
	@address = 'Rua Presbítero Aloísio Santos',
	@neighborhood = 'Farolândia',
	@city = 'São José do Rio Preto',
	@complement = '',
	@state_address = 'SP'

go*/



/* upsert book*/
create procedure upsertBook
(
	@uuid char(36),
	@employee_people_uuid char(36),
	@name varchar(50),
	@author varchar(50),
	@cover varchar(max),
	@pages varchar(5),
	@publishing_company varchar(50),
	@quantity int,
	@genre varchar(20),
	@price money,
	@isbn varchar(20),
	@synopsis nvarchar(max)
)
as
begin
		IF EXISTS(select 1 from books where books.uuid = @uuid)
		begin
			UPDATE books
					SET 
						uuid=@uuid,
						employee_people_uuid=@employee_people_uuid,
						name=@name,
						author=@author,
						cover=@cover,
						pages=@pages,
						publishing_company=@publishing_company,
						quantity=@quantity,
						genre=@genre,
						price=@price,
						isbn=@isbn,
						synopsis=@synopsis,
						updated_at=GETDATE()
			WHERE books.uuid = @uuid;
		end
	ELSE  
		begin
			INSERT INTO books( uuid, employee_people_uuid, name, author, cover,pages, publishing_company, quantity, genre ,price ,isbn ,synopsis ,created_at)
			VALUES           (@uuid,@employee_people_uuid,@name,@author,@cover,@pages,@publishing_company,@quantity,@genre,@price,@isbn,@synopsis,GETDATE());
		end
end
go


-- EXEMPLO DE EXECUÇÃO INSERT --
/*Exec upsertBook
    @uuid = 'da2dd836-0f4c-11eb-adc1-0242ac120002',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Jogos Vorazes',
	@author = 'Suzanne Collins',
	@cover = 'https://imagem.livro1',
	@pages = '400',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 20,
	@genre = 'Ficção Científica',
	@price = 27.00,
	@isbn = '8579800242',
	@synopsis = 'Após o fim da América do Norte, uma nova nação chamada Panem surge. Formada por doze distritos, é comandada com mão de ferro pela Capital. Uma das formas com que demonstram seu poder sobre o resto do carente país é com Jogos Vorazes, uma competição anual transmitida ao vivo pela televisão, em que um garoto e uma garota de doze a dezoito anos de cada distrito são selecionados e obrigados a lutar até a morte! Para evitar que sua irmã seja a mais nova vítima do programa, Katniss se oferece para participar em seu lugar. Vinda do empobrecido distrito 12, ela sabe como sobreviver em um ambiente hostil. Peeta, um garoto que ajudou sua família no passado, também foi selecionado. Caso vença, terá fama e fortuna. Se perder, morre. Mas para ganhar a competição, será preciso muito mais do que habilidade. Até onde Katniss estará disposta a ir para ser vitoriosa nos Jogos Vorazes?'

go*/

-- EXEMPLO DE EXECUÇÃO UPDATE --
/*Exec upsertBook
    @uuid = 'da2dd836-0f4c-11eb-adc1-0242ac120002',
	@employee_people_uuid = '5b2bac2c-0f4a-11eb-adc1-0242ac1200022',
	@name = 'Jogos Vorazes',
	@author = 'Suzanne Collins',
	@cover = 'https://imagem.livro1',
	@pages = '400',
	@publishing_company = 'Rocco Jovens Leitores',
	@quantity = 20,
	@genre = 'Ficção Científica',
	@price = 27.00,
	@isbn = '8579800242',
	@synopsis = ''
go*/


/* upsert carts*/
create procedure upsertCart
(
	@uuid char(36),
	@delivery_addresses_uuid char(36),
	@bought_in datetime,
	@status_carts varchar(10)
)
as
begin
		IF EXISTS(select 1 from carts where carts.uuid = @uuid)
		begin
			UPDATE carts
					SET
						delivery_addresses_uuid=@delivery_addresses_uuid,
						bought_in=@bought_in,
						status_carts=@status_carts,
						updated_at = GETDATE()
			WHERE carts.uuid = @uuid;
		end
	ELSE  
		begin
			INSERT INTO carts( uuid, delivery_addresses_uuid, bought_in, status_carts,created_at)
			VALUES           (@uuid,@delivery_addresses_uuid,@bought_in,@status_carts,GETDATE());
		end
end
go

-- Exemplo de execução Insert --
/*Exec upsertCart
	@uuid = '465eb438-0f4f-11eb-adc1-0242ac120004',
	@delivery_addresses_uuid = '33e0469e-0f4c-11eb-adc1-0242ac120002',
	@bought_in = '',
	@status_carts = 'aguardando'

go*/

-- EXEMPLO DE EXECUÇÃO UPDATE --
/*Exec upsertCart
	@uuid = '465eb438-0f4f-11eb-adc1-0242ac120004',
	@delivery_addresses_uuid = '33e0469e-0f4c-11eb-adc1-0242ac120002',
	@bought_in = '',
	@status_carts = 'finalizado'

go*/



/* upsert Items_carts*/
create procedure upsertItemCart
(
	@cart_uuid char(36),
	@book_uuid char(36),
	@quantity int,
	@price money
)
as
begin
		IF EXISTS(select 1 from items_cart where items_cart.cart_uuid = @cart_uuid AND items_cart.book_uuid = @book_uuid )
		begin
			UPDATE items_cart
					SET	
						quantity=@quantity,
						price=@price
			where items_cart.cart_uuid = @cart_uuid AND items_cart.book_uuid = @book_uuid
		end
	ELSE  
		begin
			INSERT INTO items_cart(cart_uuid, book_uuid, quantity, price)
			VALUES				  (@cart_uuid,@book_uuid,@quantity,@price);
		end
end
go
-- EXEMPLO DE EXECUÇÃO INSERT --
/*Exec upsertItemCart
	@cart_uuid = '465eb438-0f4f-11eb-adc1-0242ac120002',
	@book_uuid = 'da2dd836-0f4c-11eb-adc1-0242ac120002',
	@quantity = 4,
	@price = 50.00

go*/

-- EXEMPLO DE EXECUÇÃO UPDATE --
/*Exec upsertItemCart
	@cart_uuid = '465eb438-0f4f-11eb-adc1-0242ac120002',
	@book_uuid = 'da2dd836-0f4c-11eb-adc1-0242ac120002',
	@quantity = 3,
	@price = 35.00

go*/


/* Delete people */
create procedure deletePeople(@uuid char(36))
as
begin

	UPDATE people    SET deleted_at = GETDATE() WHERE people.uuid = @uuid
	UPDATE readers   SET deleted_at = GETDATE() WHERE readers.people_uuid = @uuid
	UPDATE employees SET deleted_at = GETDATE() WHERE employees.people_uuid = @uuid
	
end
go

-- EXEMPLO DE EXECUÇÃO SOFT DELETE --
/* exec deletePeople
	@uuid = '182ba040-0f49-11eb-adc1-0242ac120002'
*/


create procedure deleteAddress(@uuid char(36))
as 
begin
	UPDATE delivery_addresses SET deleted_at = GETDATE() WHERE delivery_addresses.uuid = @uuid
end
go
-- EXEMPLO DE EXECUÇÃO SOFT DELETE --
/* exec deleteAdress
	@uuid = '33e0469e-0f4c-11eb-adc1-0242ac120002'
*/


create procedure deleteBook(@uuid char(36))
as 
begin
	UPDATE books      SET deleted_at = GETDATE() WHERE books.uuid = @uuid
	DELETE FROM items_cart WHERE book_uuid = @uuid
end
go
-- EXEMPLO DE EXECUÇÃO SOFT DELETE --
/* exec deleteBook
	@uuid = '63a4c060-0f4e-11eb-adc1-0242ac120002'
*/



create procedure deleteItemsCart(@cart_uuid char(36), @book_uuid char(36))
as 
begin
	DELETE FROM items_cart WHERE cart_uuid = @cart_uuid AND book_uuid = @book_uuid
end
go

-- EXEMPLO DE EXECUÇÃO SOFT DELETE --
/* exec deleteItemsCart
	@cart_uuid = '465eb438-0f4f-11eb-adc1-0242ac120004',
	@book_uuid = 'e5184bc2-0f4d-11eb-adc1-0242ac120002'
*/

create procedure updateQuantityBook
(
	@uuid char(36),
	@quantity int
)
as
begin
		IF EXISTS(select 1 from books where books.uuid = @uuid)
		begin
			UPDATE books
					SET 
						quantity=@quantity,
						updated_at=GETDATE()
			WHERE books.uuid = @uuid;
		end
end
go

-- EXEMPLO DE EXECUÇÃO update  Quantity Book --
/* exec deleteItemsCart
	@uuid = '465eb438-0f4f-11eb-adc1-0242ac120004',
	@quantity = '42'
*/


create procedure updatePriceBook
(
	@uuid char(36),
	@price money
)
as
begin
		IF EXISTS(select 1 from books where books.uuid = @uuid)
		begin
			UPDATE books
					SET 
						price=@price,
						updated_at=GETDATE()
			WHERE books.uuid = @uuid;
		end
end
go 

-- EXEMPLO DE EXECUÇÃO update  Quantity Book --
/* exec deleteItemsCart
	@uuid = '465eb438-0f4f-11eb-adc1-0242ac120004',
	@price = '50.90'
*/

create procedure slectReportSalesMonth 
(
	@month int,
	@year int
)
as
	SELECT 
		SUM(items_cart.quantity) sold_books,
		SUM(items_cart.quantity * items_cart.price) revenue,
		COUNT(DISTINCT books.uuid) new_books
	FROM
		items_cart
		INNER JOIN carts ON items_cart.cart_uuid = carts.uuid
		RIGHT JOIN books ON items_cart.book_uuid = books.uuid
	WHERE
	 datepart(month, carts.bought_in) = @month and datepart(year,carts.bought_in) = @year and
	 carts.status_carts = 'finalizado'
go

-- EXEMPLO DE EXECUÇÃO procedure slectReportSalesMonth 
 --
/* exec deleteItemsCart
	"month =11,
	@year =2020
*/