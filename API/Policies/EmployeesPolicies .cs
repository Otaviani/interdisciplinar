using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

using API.Data;
using API.Models;
using API.Shared;

namespace API.Policies
{
	public class EmployeesPolicies  
	{	
		public EmployeesPolicies ()
		{
		}

        public void isEmployee(string token){
            string uuid =  Services.TokenService.decodeToken(token);
            EmployeeData employeeData = new EmployeeData();
            Boolean exist = employeeData.ExistEmployeeActiveByUuid(uuid);
            if(!exist)
                throw new System.InvalidOperationException("Acesso apenas para funcionarios!");
        }

        public void isAdministrator(string token){
            string uuid =  Services.TokenService.decodeToken(token);
            EmployeeData employeeData = new EmployeeData();
            Boolean exist = employeeData.ExistEmployeeAdministratorByUuid(uuid);
            if(!exist)
                throw new System.InvalidOperationException("Acesso apenas para funcionarios!");
        }
	}
}
