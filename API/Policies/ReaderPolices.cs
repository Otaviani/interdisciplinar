
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

using API.Data;
using API.Models;
using API.Shared;

namespace API.Policies
{
	public class ReaderPolices  
	{	
		public ReaderPolices()
		{
		}
         public void AlterData(string token, string pUuid)
        {
            string uuidLogged =  Services.TokenService.decodeToken(token); 
            isReader(uuidLogged);
            UuidIsEqualToToken(uuidLogged,pUuid);
        }
        public void AlterDataPatch(string token, string pUuid)
        {
            string uuidLogged =  Services.TokenService.decodeToken(token); 
            isReader(uuidLogged);
            UuidIsEqualToToken(uuidLogged,pUuid);
        }
        public void GetAddressesByReader(string token,string pUuid){
            string uuidLogged =  Services.TokenService.decodeToken(token);
            isReader(uuidLogged);
            UuidIsEqualToToken(uuidLogged, pUuid);
        }

        public void AlterDataDeleteAddress(string token, string reader_people_uuid){
            string uuidLogged =  Services.TokenService.decodeToken(token);
            isReader(uuidLogged);
            UuidIsEqualToToken(uuidLogged, reader_people_uuid);
        }

        public void isReader(string uuid){
            ReaderData readerData = new ReaderData();
            Boolean exist = readerData.ExistReader(uuid);
            if(!exist)
                throw new System.InvalidOperationException("Voce não tem permição para isso!");
        }

        public void UuidIsEqualToToken(string puuidLogged, string pUuid){
            if(puuidLogged != pUuid)
                throw new System.InvalidOperationException("Voce não tem permição ver informação de outra pessoa!");
        }
	}
}
