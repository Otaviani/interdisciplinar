using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

using API.Data;
using API.Models;
using API.Shared;

namespace API.Policies
{
	public class BooksPolicies 
	{	
		public BooksPolicies()
		{
		}

        public void getReportSales(string token){
            isEmployee(token);
        }
        public void GetListBooksSettings(string token){
            isEmployee(token);
        }
        public void FindBookByIsbn(string token)
        {
            isEmployee(token);
        }

        public void FormDataUpload(string token)
        {
            isEmployee(token);
        }
        public void UpdateQuantityBook(string token)
        {
            isEmployee(token);
        }
        public void UpdatePriceBook(string token)
        {
            isEmployee(token);
        }
        public void isEmployee(string token){
            string uuid =  Services.TokenService.decodeToken(token);
            EmployeeData employeeData = new EmployeeData();
            Boolean exist = employeeData.ExistEmployeeActiveByUuid(uuid);
            if(!exist)
                throw new System.InvalidOperationException("Acesso apenas para funcionarios!");
        }
    }
}
