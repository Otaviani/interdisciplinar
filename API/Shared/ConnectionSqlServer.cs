using System;
using System.Data;
using System.Data.SqlClient;

namespace API.Shared
{    
    public abstract class ConnectionSqlServer : IDisposable
    {
        protected SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
        protected SqlConnection connectionBD = new SqlConnection();

        protected void openConnectionSqlServer(){
            try{
                string ConnectionString = builder.ConnectionString;
                this.connectionBD = new SqlConnection(ConnectionString);
                this.connectionBD.Open();
            }catch (SqlException ex){
                Console.WriteLine("Error connecting to database: " + ex);
                throw ex;
            }
        }

        protected SqlDataReader ReadComand(SqlCommand pCommand){
            openConnectionSqlServer();
            pCommand.Connection = this.connectionBD;
            SqlDataReader reader = pCommand.ExecuteReader();
            return reader;
        }
        
        protected void insertComand(SqlCommand pCommand){
            openConnectionSqlServer();
            SqlCommand cmd = pCommand;
            cmd.Connection = this.connectionBD;
            cmd.ExecuteNonQuery();
        }

        protected SqlCommand BuildSqlCommandTypeStoredProcedure(String pNameProcedure){
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = pNameProcedure;
            cmd.CommandType = CommandType.StoredProcedure;
            return cmd;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
           if (disposing){ 
               Console.WriteLine("Close connection");
               this.connectionBD.Close();
           }
        }
    }
}