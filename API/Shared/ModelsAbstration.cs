using System;

namespace API.Shared
{    
    public abstract class ModelsAbstration 
    {      
        public DateTime created_at { get; set; }
	    public DateTime updated_at { get; set; }
	    public DateTime deleted_at { get; set; }
        public bool CheckValidDateTime(DateTime pDateTime)
        {
            int year = pDateTime.Year;
            int month = pDateTime.Month;
            int day = pDateTime.Day;

            bool check = false;
            if(	year <= DateTime.MaxValue.Year && year >= DateTime.MinValue.Year ){
                if( month >= 1 && month <= 12 ){
                    if ( DateTime.DaysInMonth( year, month ) >= day && day >= 1 )
                        check = true;
                }
            }
            return check;   
        }
    }
}