using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;

using API.Models;
using API.Data;

namespace API.Services
{
    public class BookService
    {
        private BookData bookData {get; set;}
        private EmployeeData employeeData {get; set;}

        public BookService(){
            bookData = new BookData();
            employeeData = new EmployeeData();
        }
        public dynamic GetReportSalesMonthCompare(){
            DateTime date = DateTime.Now;

            int cuncorrentMonth = date.Month;
            int cuncorrentYear = date.Year;

            int compareMonth = cuncorrentMonth - 1;
            int compareYear = cuncorrentYear;
            
            if (compareMonth == 0){
                compareMonth = 12;
                compareYear = compareYear - 1;
            }

            ReportSalesMonth cuncorrentReportSalesMonth = bookData.slectReportSalesMonth(cuncorrentMonth, cuncorrentYear);
            ReportSalesMonth compareReportSalesMonth = bookData.slectReportSalesMonth(compareMonth, compareYear);
            
            List<ReportSalesBooks>  cuncorrentSalesBooks = bookData.GetReportSalesBooksByMonthAndYear(cuncorrentMonth, cuncorrentYear);
            List<ReportSalesBooks>  compareSalesBooks = bookData.GetReportSalesBooksByMonthAndYear(compareMonth, compareYear);
            List<ReportCompareSalesBooks> reportSales = new List<ReportCompareSalesBooks>();

            foreach(ReportSalesBooks reportSalesBooks in cuncorrentSalesBooks){
                reportSales.Add( new ReportCompareSalesBooks(reportSalesBooks,ref compareSalesBooks) );
            }

            foreach(ReportSalesBooks reportSalesBooks in compareSalesBooks){
                reportSales.Add( new ReportCompareSalesBooks(reportSalesBooks) );
            }

            dynamic reportSalesMonth = new {
                sold_books = cuncorrentReportSalesMonth.sold_books,
                revenue = cuncorrentReportSalesMonth.revenue,
                last_month_revenue = compareReportSalesMonth.revenue,
                new_books = cuncorrentReportSalesMonth.new_books
            };

            return new{
                reportSalesMonth,
                reportSales
            };
        }

        public List<string> GetGenres(){
            return bookData.GetGenres();
        }

        public void newBook(Book pBook, string pUuidEmployee){
            Boolean isEmployee = employeeData.ExistEmployeeActiveByUuid(pUuidEmployee);
            if(!isEmployee)
				throw new System.InvalidOperationException("Ação restrita para pessoas autorizadas");

            pBook.uuid  = System.Guid.NewGuid().ToString();
            pBook.employee_people_uuid = pUuidEmployee;
            
            UpdateBook(pBook);
        }

        public void UpdateBook(Book pBook)
        {
            pBook.cover = "";
            if( bookData.BookExistsByUuid(pBook.uuid)){
                Book currentInfos = bookData.GetBookByUuid(pBook.uuid);
                pBook.cover = currentInfos.cover;
            }

            if (pBook.coverFile != null){
                if(pBook.coverFile.Length >0){
                    string directoryBook = "books/"+pBook.uuid;
                    Services.FilesService.uploudFile(directoryBook,pBook.coverFile);
                    pBook.cover = "http://localhost:5000/storage/books/"+pBook.uuid+"/"+pBook.coverFile.FileName;
                }
            }

            UpsertBook(pBook);
        }
        public void UpsertBook(Book pBook){

                if (pBook.isbn == null)
                    throw new System.InvalidOperationException("Isbn é obrigatorio!");
                if (bookData.BookExistsByIsbnAndNotIsUuid(pBook.isbn,pBook.uuid))
                    throw new System.InvalidOperationException("Isbn já está cadastrado.");
                if (pBook.employee_people_uuid == null)
                    throw new System.InvalidOperationException("Informe quem criou o livro.");
                if (pBook.name == null)
                    throw new System.InvalidOperationException("Nome do livro é obrigatorio.");
                if (pBook.author == null)
                    throw new System.InvalidOperationException("Autor do livro é obrigatorio.");
                if (pBook.pages == null)
                    throw new System.InvalidOperationException("Quantida de paginas do livro é obrigatorio.");
                if (pBook.publishing_company == null)
                    throw new System.InvalidOperationException("Editora do livro é obrigatorio.");
                if (pBook.quantity < 0)
                    throw new System.InvalidOperationException("Quantidade do livro não pode ser negativa!");
                if (pBook.price < 0)
                    throw new System.InvalidOperationException("Quantidade do livro não pode ser negativa!");
                if (pBook.genre == null)
                    throw new System.InvalidOperationException("Genero do livro é obrigatorio.");
                
                bookData.upsertBook(pBook);
        }

        public void UpdateQuantityBook(string pUuid, int pQuantity){
            if(pQuantity < 0)
                throw new System.InvalidOperationException("Quantidade é negativa");
            bookData.UpdateQuantityBook(pUuid,pQuantity);
        }
        public void UpdatePriceBook(string pUuid, Decimal pPrice){
            if(pPrice < 0)
                throw new System.InvalidOperationException("Preço é negativo");
            bookData.UpdatePriceBook(pUuid,pPrice);
        }
        
        public Book GetBookByUuid(string pUuid){
            return bookData.GetBookByUuid(pUuid);
        }

        public List<Book> GetListBooksSettings(){
            List<Book> listBooksSettings = bookData.GetAll();
            return listBooksSettings;
        }

        public List<Book> GetListBooksToBuy(){
            List<Book> last5BookReleases = bookData.GetLast5BookReleases();
            List<Book> withStock = bookData.GetBooksWithStock();
            List<Book> booksWithoutStock = bookData.GetBooksWithoutStock();

            List<Book> report = new List<Book>();
            report.AddRange(last5BookReleases);
            report.AddRange(withStock);
            report.AddRange(booksWithoutStock);

            return report;
        }

        public void DeleteBook(string pUuid)
        {
            bookData.DeleteBook(pUuid);
        }
    }
}