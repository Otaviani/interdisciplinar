using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

using System.Collections.Generic;
using API.Models;
using API.Data;


namespace API.Services
{
    public class DeliveryAddressService
    {   
        private DeliveryAddressData deliveryAddressData;
        
        public DeliveryAddressService(){
            deliveryAddressData = new DeliveryAddressData();
        }

        public DeliveryAddress Create(DeliveryAddress pDelivery){
            pDelivery.uuid =  System.Guid.NewGuid().ToString();
            Save(pDelivery);
            return pDelivery;
        }
        public void Updade(DeliveryAddress pDelivery){
            Save(pDelivery);
        }

        public void Save(DeliveryAddress pDelivery){
            ValidDelivery(pDelivery);
            if (pDelivery.uuid == null)
                throw new System.InvalidOperationException("UUID é obrigatorio.");
            deliveryAddressData.Upsert(pDelivery);
        }

        public void Delete(string uuidAddress){
            deliveryAddressData.Delete(uuidAddress);
        }

        public void ValidDelivery(DeliveryAddress pDelivery){

            if (pDelivery.complement == null)
                pDelivery.complement = "";

            if (pDelivery.reader_people_uuid == null)
                throw new System.InvalidOperationException("reader_people_uuid é obrigatorio.");
            if (pDelivery.number == null)
                throw new System.InvalidOperationException("Numero é obrigatorio.");
            if (pDelivery.cep == null)
                throw new System.InvalidOperationException("CEP é obrigatorio.");
            if (pDelivery.address == null)
                throw new System.InvalidOperationException("Endereço é obrigatorio.");
            if (pDelivery.neighborhood == null)
                throw new System.InvalidOperationException("Bairro é obrigatorio.");
            if (pDelivery.city == null)
                throw new System.InvalidOperationException("Cidade é obrigatorio.");
            if (pDelivery.state_address == null)
                throw new System.InvalidOperationException("Estado é obrigatorio.");
        }

        public List<DeliveryAddress>  GetAddressesByReaderUuid(string readerUuid){
            return deliveryAddressData.GetListDeliveryAddressDataByPeople_uuid(readerUuid);
        }

        public DeliveryAddress GetAddressesByUuid(String pUuid){
            return deliveryAddressData.GetAddressesByUuid(pUuid);
        }
    }
}