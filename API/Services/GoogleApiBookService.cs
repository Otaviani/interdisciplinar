using System;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;

using API.Models;
using API.Data;


namespace API.Services
{
    public class GoogleApiBookService
    {
        private const string URL = "https://www.googleapis.com/books/v1/volumes";
        private const string KEY = "AIzaSyDrdrzXCjbrosh9mEGWKZeHsc7f2JhR0QA";
        public GoogleApiBookService(){
        }

        public Book GetBookInfo(string pIsbn)
        {
            string uri = String.Format("{0}?q=isbn:{1}&key={2}",URL,pIsbn,KEY);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

            request.Method = "GET";
            request.ContentType = "application/json";

            WebResponse webResponse = request.GetResponse();
            Stream webStream = webResponse.GetResponseStream();
            StreamReader responseReader = new StreamReader(webStream);
            dynamic json =  JObject.Parse(responseReader.ReadToEnd());
            responseReader.Close();

            return new Book{ 
                isbn               = pIsbn,
                name               = json.items[0].volumeInfo.title,
                pages              = json.items[0].volumeInfo.pageCount,
                author             = json.items[0].volumeInfo.authors[0],
                publishing_company = json.items[0].volumeInfo.publisher,
                synopsis           = json.items[0].volumeInfo.description
            };
        }


    }
}