using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;

using API.Models;
using API.Data;

namespace API.Services
{
    public class CartService
    {
        private CartData cartData {get; set;}
        private BookService bookService {get; set;}

        public CartService(){
            cartData = new CartData();
            bookService = new BookService();
        }
        public void buyCart(string pDeliveryAddressUuid, List<BookToBuy> pItens){

            Cart cart = new Cart();
            List<Book> booksToUpdate = new List<Book>();
            List<ItemCart> itemsCart = new List<ItemCart>();

            cart.uuid = System.Guid.NewGuid().ToString();
            cart.delivery_addresses_uuid = pDeliveryAddressUuid;
            cart.bought_in = DateTime.Now;
            cart.status_carts = "finalizado";            

            foreach(BookToBuy item in pItens){
                ItemCart intemCart = new ItemCart();
                Book book = bookService.GetBookByUuid(item.uuid);

                intemCart.book_uuid = book.uuid;
                intemCart.quantity  = item.quantity;
                intemCart.price     = book.price;

                if (book.quantity < item.quantity){
                    string mensage = string.Format("O livro {0} tem apenas {1} unidades em estoque. Não é possivel comprar {2} unidades neste momento",book.name,book.quantity,item.quantity);
                    throw new System.InvalidOperationException(mensage);
                }
                book.quantity = book.quantity - item.quantity;

                booksToUpdate.Add(book);
                itemsCart.Add(intemCart);
            }

            cartData.upsertCart(cart);

            foreach(Book bookUpdate in booksToUpdate){
                bookService.UpdateQuantityBook(bookUpdate.uuid, bookUpdate.quantity);
            }

            foreach(ItemCart item in itemsCart){
                item.cart_uuid = cart.uuid;
                cartData.insterItemCart(item);
            }

        }

    }
}