using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

using System.Collections.Generic;
using API.Models;
using API.Data;

namespace API.Services
{
    public class ReaderService
    {
        private ReaderData ReaderData {get; set;}
        public ReaderService(){
            ReaderData = new ReaderData();
        }
        public Reader GetReaderByLoginPassword(String pLogin, String pPassword){
            return ReaderData.GetReaderByLoginPassword(pLogin, pPassword);
        }
        public void Create(Reader reader,DeliveryAddress deliveryAddress){
            DeliveryAddressService deliveryAddressService = new DeliveryAddressService();

            if(reader.uuid == null)
                reader.uuid =  System.Guid.NewGuid().ToString();
            if(reader.uuid.Length < 5)
                reader.uuid =  System.Guid.NewGuid().ToString();

            reader.login = reader.email;
            deliveryAddress.reader_people_uuid = reader.uuid;

            deliveryAddressService.ValidDelivery(deliveryAddress);

            Save(reader);
            deliveryAddressService.Create(deliveryAddress);
        }

        public void Update(Reader reader){
            Reader currentInformations = ReaderData.GetReaderByUuid(reader.uuid);

            if(reader.login == null)
                reader.login = currentInformations.login;
            if(reader.password == null)
                reader.password = currentInformations.password;
            if (reader.photo == null)
                reader.photo = currentInformations.photo;
            if (reader.about == null )
                reader.about = currentInformations.about;
            
            Save(reader);
        }

        public Reader GetReaderByUuid(string uuid)
        {
            return ReaderData.GetReaderByUuid(uuid);
        }

        public void Save(Reader reader){
            
            if(reader.uuid == null)
                throw new System.InvalidOperationException("uuid é obrigatorio");
            if(reader.name == null)
                throw new System.InvalidOperationException("nome é obrigatorio");
            if(reader.cpf == null)
                throw new System.InvalidOperationException("cpf é obrigatorio");
            if(reader.birth == null)
                throw new System.InvalidOperationException("Data de Nascimento é obrigatorio");
            if (!reader.CheckValidDateTime(reader.birth))
                throw new System.InvalidOperationException("Data de Nascimento é invalido");
            if(reader.email == null)
                throw new System.InvalidOperationException("Email é obrigatorio");
            if(reader.login == null)
                throw new System.InvalidOperationException("nome é obrigatorio");
            if(reader.password == null)
                throw new System.InvalidOperationException("nome é obrigatorio");
                
            if (reader.about == null)
                reader.about = "";
            if(reader.photo == null)
                reader.photo = "";

            if (reader.photoFile != null){
                if(reader.photoFile.Length > 0){
                    string directoryPhoto = "people/" + reader.uuid;
                    Services.FilesService.uploudFile(directoryPhoto,reader.photoFile);
                    reader.photo = "http://localhost:5000/storage/people/"+reader.uuid+"/"+reader.photoFile.FileName;
                }
            }
            ReaderData.upsertReader(reader);
        }
    }
}