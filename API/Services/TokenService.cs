using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

using System.Collections.Generic;
using API.Models;
using API.Data;

namespace API.Services
{
    public static class TokenService
    {

        public static string decodeToken(string authHeader)
        {
            var handler = new JwtSecurityTokenHandler();
            authHeader = authHeader.Replace("Bearer ", "");
            JwtSecurityToken tokenS = handler.ReadToken(authHeader) as JwtSecurityToken;
            String uuid = tokenS.Claims.First(claim => claim.Type == "uuid").Value;
            return uuid;
        }

        public static string GenerateToken(People people, Boolean isEmployee)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Settings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("uuid", people.uuid.ToString()),
                    new Claim("name", people.name.ToString()),
                    new Claim("photo", people.photo.ToString()),
                    new Claim("is_employee", isEmployee.ToString())
                }),
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature
                ),
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

    }
}