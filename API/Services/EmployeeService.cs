using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

using System.Collections.Generic;
using API.Models;
using API.Data;


namespace API.Services
{
    public class EmployeeService
    {   
        private EmployeeData employeeData;
        
        public EmployeeService(){
            employeeData = new EmployeeData();
        }

        public void DeleteEmployee(String pUuid){
            Employee employee = employeeData.GetEmployeeByUuid(pUuid);
            employee.employee_status = "demitido";
            UpSert(employee);
        }

        public void UpdateEmployee(Employee employeeUpdate)
        {

            Employee currentInformations = employeeData.GetEmployeeByUuid(employeeUpdate.uuid);
   
            if (employeeUpdate.about == null )
                employeeUpdate.about = currentInformations.about;

            if (employeeUpdate.login == null )
                employeeUpdate.login = currentInformations.login;

            if (employeeUpdate.password == null)
                employeeUpdate.password = currentInformations.password;
            
            if (employeeUpdate.employee_status == null)
                employeeUpdate.employee_status = currentInformations.employee_status;

            if (employeeUpdate.employee_type == null)
                employeeUpdate.employee_type = currentInformations.employee_type;

            if (employeeUpdate.photo == null)
                employeeUpdate.photo = currentInformations.photo;

            if (employeeUpdate.photoFile != null){
                if(employeeUpdate.photoFile.Length > 0){
                    string directoryPhoto = "people/" + employeeUpdate.uuid;
                    Services.FilesService.uploudFile(directoryPhoto,employeeUpdate.photoFile);
                    employeeUpdate.photo = "http://localhost:5000/storage/people/"+employeeUpdate.uuid+"/"+employeeUpdate.photoFile.FileName;
                }
            }

            UpSert(employeeUpdate);
        }

        public void Create(Employee employee, DeliveryAddress Address)
        {
            ReaderService readerService = new ReaderService();

            Reader reader = new Reader(employee);
            reader.uuid = System.Guid.NewGuid().ToString();

            readerService.Create(reader,Address);

            employee.uuid = reader.uuid;
            employee.login = employee.email;
            employee.photo = reader.photo;
            employee.about = reader.about;
            employee.employee_status = "ativo";

            UpSert(employee);
        }

        public void UpSert(Employee pEmployee){

            if (pEmployee.employee_status.Length <= 1)
                throw new System.InvalidOperationException("Status do funcionario 'ativo', 'demitido', 'suspenso' é obrigatorio");

            if (pEmployee.employee_type.Length <= 1)
                throw new System.InvalidOperationException("Tipo do funcionario 'administrador', 'colaborador' é obrigatorio");

            if (pEmployee.name.Length <= 1)
                throw new System.InvalidOperationException("nome é obrigatorio");

            if (pEmployee.cpf.Length <= 1)
                throw new System.InvalidOperationException("cpf é obrigatorio");

            if (pEmployee.email.Length <= 1)
                throw new System.InvalidOperationException("email é obrigatorio");

            if (!pEmployee.CheckValidDateTime(pEmployee.birth))
                throw new System.InvalidOperationException("data de aniversário é invalida!");

            employeeData.UpsertEmployee(pEmployee);
        }

        public List<Employee> GetAllEmployees(){
            return employeeData.GetAllEmployees();
        }

        public Employee GetEmployeeByUuid(string pUuid)
        {
            return employeeData.GetEmployeeByUuid(pUuid);
        }
        public Boolean IsEmployee(string pUuid){
           return employeeData.ExistEmployeeActiveByUuid(pUuid);
        }
    }
}