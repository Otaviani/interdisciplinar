using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace API.Services
{
    public static class FilesService
    {
        public static async void uploudFile(string pDirectory, IFormFile pFormFile){
            if (pFormFile.Length > 0){
                pDirectory = Path.Combine("wwwroot","storage", pDirectory);
                Directory.CreateDirectory(pDirectory);
                var filePath = Path.Combine(pDirectory, pFormFile.FileName);
                using (var stream = System.IO.File.Create(filePath)){   await pFormFile.CopyToAsync(stream);    }
            }
        }
    }
}