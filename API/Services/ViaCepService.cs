using System;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Collections.Generic;

using API.Models;
using API.Data;

namespace API.Services
{
    public class ViaCepService
    {
        private const string URL = "https://viacep.com.br/ws";

        public ViaCepService(){
        }

        public DeliveryAddress GetAddress(string pCep)
        {
            String uri = String.Format("{0}/{1}/json",URL,pCep);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

            request.Method = "GET";
            request.ContentType = "application/json";

            WebResponse webResponse = request.GetResponse();
            Stream webStream = webResponse.GetResponseStream();
            StreamReader responseReader = new StreamReader(webStream);
            dynamic json =  JObject.Parse(responseReader.ReadToEnd());
            responseReader.Close();

            return new DeliveryAddress{
                cep = pCep,
                city = json.localidade,
                neighborhood = json.bairro,
                state_address = json.uf,
                address = json.logradouro
            };
        }
    }
}