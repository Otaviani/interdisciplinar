using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using API.Shared;

namespace API.Models
{
	public class RequestToBuyCart
	{
        public string address_uuid { get; set; }
        public List<BookToBuy> BooksToBuy {  get; set; } 
		public RequestToBuyCart()
		{

		}
	}
}
