using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

using API.Shared;

namespace API.Models
{
    public class ReportCompareSalesBooks
    {
		public string uuid = "";
        public string name = "";
        public int sales = 0;
        public int last_month_sales = 0;
        public int sales_diff = 0;
        public decimal gain = 0;
        public decimal last_month_gain = 0;
        public decimal gain_diff = 0;
        public ReportCompareSalesBooks(){
        }

        public ReportCompareSalesBooks(ReportSalesBooks compareReportSalesBooks){
            uuid  = compareReportSalesBooks.uuid;
            name  = compareReportSalesBooks.name;
            last_month_sales = compareReportSalesBooks.sales;
            last_month_gain  = compareReportSalesBooks.gain;

            sales_diff = last_month_sales * -1;
            gain_diff  = last_month_gain  * -1;
        }

        public ReportCompareSalesBooks(ReportSalesBooks reportSalesBooks, ref List<ReportSalesBooks> listCompareReportSalesBooks ){

            uuid  = reportSalesBooks.uuid;
            name  = reportSalesBooks.name;
            sales = reportSalesBooks.sales;
            gain  = reportSalesBooks.gain;

            int index = 0;
            foreach(ReportSalesBooks compareReportSalesBooks in listCompareReportSalesBooks){
                if( reportSalesBooks.uuid == compareReportSalesBooks.uuid){
                    last_month_sales = compareReportSalesBooks.sales;
                    last_month_gain = compareReportSalesBooks.gain;
                    sales_diff = sales - last_month_sales;
                    gain_diff = gain - last_month_gain;
                    listCompareReportSalesBooks.RemoveAt(index);
                    break;
                }
                 index++;
            }
            

        }

    }
}