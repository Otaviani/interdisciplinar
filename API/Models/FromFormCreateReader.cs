using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

using API.Shared;

namespace API.Models
{
    public class FromFormCreateReader : ModelsAbstration
    {
        public string uuid { get; set; }
        public string name { get; set; }
	    public string cpf{ get; set; }
	    public string login{ get; set; }
	    public string password{ get; set; }
        public String photo{ get; set; }
        public IFormFile photoFile{ get; set; }
        public string telephone { get; set; }
        public string email { get; set; }
        public string about { get; set; }
        public DateTime birth{ get; set; }
        public string cep{ get; set; }
        public string city{ get; set; }
        public string state{ get; set; }
        public string street{ get; set; }
        public string neighborhood{ get; set; }
        public string number{ get; set; }
        public string complement{ get; set; }
        public FromFormCreateReader()
        {
			
        }
    }
}