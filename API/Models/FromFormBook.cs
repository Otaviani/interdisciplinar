using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

using API.Shared;

namespace API.Models
{
    public class FromFormBook : ModelsAbstration
    {
        public string uuid { get; set; }
	    public string employee_people_uuid  { get; set; }
	    public string name { get; set; }
	    public string author { get; set; }
	    public string pages { get; set; }
	    public string publishing_company { get; set; }
	    public string quantity { get; set; }
	    public string genre { get; set; }
	    public string price { get; set; }
	    public string isbn { get; set; }
	    public string cover { get; set; }
		public string synopsis { get; set; }
		public IFormFile coverFile { get; set; }
        public FromFormBook()
        {
			
        }
    }
}