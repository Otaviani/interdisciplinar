using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API.Shared;

namespace API.Models
{    public class EmployeeType : EnumAbstration
    {
        public string ADMINISTRATOR  { get { return "administrador"; } }
        public string COLLABORATOR  { get { return  "colaborador"; } }
    }
    public class EmployeeStatusEnum : EnumAbstration
    {
        public string ACTIVE  { get { return  "ativo"; } }
        public string FIRED  { get { return  "demitido"; } }
        public string SUSPENDED  { get { return  "suspenso"; } }

    }

    public class Employee : People
    {   
        public string telephone { get; set; }
        public string email { get; set; }
        public string about { get; set; }

        public string employee_type { get; set; }
        public string employee_status { get; set; }

        public Employee( )
        {
        }

        public Employee(FromFormCreateEmployee employeeForm )
        {
            name  =employeeForm.name;
            cpf   =employeeForm.cpf;
            birth =employeeForm.birth;
            login =employeeForm.login;
            password =employeeForm.password;
            photo =employeeForm.photo;
            photoFile =employeeForm.photoFile;
            telephone =employeeForm.telephone;
            email =employeeForm.email;
            employee_type =employeeForm.employee_type;
        }

        public Employee(SqlDataReader result )
        {
            uuid                    = (string)   result["uuid"];
            name                    = (string)   result["name"];
	        cpf                     = (string)   result["cpf"];
	        login                   = (string)   result["login"];
	        password                = (string)   result["password"];
            telephone               = (string)   result["telephone"];
            email                   = (string)   result["email"];
            about                   = (string)   result["about"];
            photo                   = (string)   result["photo"];
            birth                   = (DateTime) result["birth"];
            employee_type     =  (string) result["employee_type"];
            employee_status   =  (string) result["employee_status"];
        }

    }
}