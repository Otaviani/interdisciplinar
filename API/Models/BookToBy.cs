using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using API.Shared;

namespace API.Models
{
	public class BookToBuy
	{
        public string uuid { get; set; }
		public int quantity { get; set; }

		public BookToBuy()
		{

		}
	}
}
