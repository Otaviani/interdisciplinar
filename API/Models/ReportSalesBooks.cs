using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

using API.Shared;

namespace API.Models
{
    public class ReportSalesBooks
    {
		public string uuid;
		public string name;
		public int sales;
		public decimal gain;
	
        public ReportSalesBooks()
        {

        }

		public ReportSalesBooks(SqlDataReader result)
        {
            uuid      = (string)   result["uuid"];
            name      = (string)   result["name"];
            sales     = (int)      result["sales"];
            gain      = (decimal)  result["gain"];
        }
    }
}