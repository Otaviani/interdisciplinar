using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using API.Shared;
using Microsoft.AspNetCore.Http;

namespace API.Models
{
    public class People : ModelsAbstration
    {
        public string uuid { get; set; }
        public string name { get; set; }
	    public string cpf{ get; set; }
	    public DateTime birth{ get; set; }
	    public string login{ get; set; }
	    public string password{ get; set; }
        public String photo{ get; set; }
        public IFormFile photoFile{ get; set; }
        public People(){
            
        }

    }
}    

