﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using API.Shared;
namespace API.Models
{
	public class Cart : ModelsAbstration
	{
		public string uuid { get; set; }
		public string delivery_addresses_uuid { get; set; }
		public DateTime bought_in { get; set; }
		public string status_carts { get; set; }
		public decimal price { get; }
		public Cart()
		{

		}
	}
}
