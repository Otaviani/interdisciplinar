using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API.Models;

namespace API.Models
{
    public class Reader : People
    {
        public string telephone { get; set; }
        public string email { get; set; }
        public string about { get; set; }
        public Reader(){

        }
        public Reader(Employee employee){
            name  =employee.name;
            cpf   =employee.cpf;
            birth =employee.birth;
            login =employee.login;
            password =employee.password;
            photo =employee.photo;
            photoFile =employee.photoFile;
            telephone =employee.telephone;
            email =employee.email;
        }

        public Reader(FromFormCreateReader readerForm){
            name  =readerForm.name;
            cpf   =readerForm.cpf;
            birth =readerForm.birth;
            login =readerForm.login;
            password =readerForm.password;
            photo =readerForm.photo;
            photoFile =readerForm.photoFile;
            telephone =readerForm.telephone;
            email =readerForm.email;
        }

        public Reader(SqlDataReader result )
        {
            uuid      = (string)   result["uuid"];
            name      = (string)   result["name"];
	        cpf       = (string)   result["cpf"];
	        birth     = (DateTime) result["birth"];
	        login     = (string)   result["login"];
	        password  = (string)   result["password"];
            telephone = (string)   result["telephone"];
            email     = (string)   result["email"];
            about     = (string)   result["about"];
            photo     = (string)   result["photo"];
        }
    }
}