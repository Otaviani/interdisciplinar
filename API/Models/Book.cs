using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

using API.Shared;
using System.Globalization;

namespace API.Models
{
    public class Book : ModelsAbstration
    {
        public string uuid { get; set; }
	    public string employee_people_uuid  { get; set; }
	    public string name { get; set; }
	    public string author { get; set; }
	    public string pages { get; set; }
	    public string publishing_company { get; set; }
	    public int quantity { get; set; }
	    public string genre { get; set; }
	    public decimal price { get; set; }
	    public string isbn { get; set; }
	    public string cover { get; set; }
		public string synopsis { get; set; }
		public IFormFile coverFile { get; set; }
        public Book()
        {

        }
		 public Book(FromFormBook FormBook)
        {
			uuid = FormBook.uuid;
			employee_people_uuid  = FormBook.uuid;
			name = FormBook.name;
			author = FormBook.author;
			pages  = FormBook.pages;
			publishing_company = FormBook.publishing_company;
			genre = FormBook.genre;
			isbn = FormBook.isbn;
			cover = FormBook.cover;
			synopsis = FormBook.synopsis;

			quantity  = Convert.ToInt32(FormBook.quantity);

			CultureInfo culture = new CultureInfo("en-US");
			price 	  = Convert.ToDecimal(FormBook.price,culture);

			coverFile = FormBook.coverFile;
        }
		public Book(SqlDataReader result )
        {
 			uuid 				  	= (string) 	result["uuid"];
		    employee_people_uuid	= (string)	result["employee_people_uuid"];
		    name 				  	= (string) 	result["name"];
	    	author 				  	= (string)	result["author"];
	    	pages 				  	= (string)	result["pages"];
	    	publishing_company 		= (string)	result["publishing_company"];
	    	quantity 				= (int) 	result["quantity"];
	    	genre					= (string)	result["genre"];
	    	price					= (decimal) result["price"];
	    	isbn 					= (string)	result["isbn"];
	    	cover 					= (string)	result["cover"];
			synopsis 				= (string)	result["synopsis"];

			if ( result["created_at"] != System.DBNull.Value )
				created_at = (DateTime) result["created_at"];

			if ( result["updated_at"] != System.DBNull.Value )
				updated_at = (DateTime) result["updated_at"];

			if ( result["deleted_at"] != System.DBNull.Value )
				deleted_at = (DateTime) result["deleted_at"];

        }
    }
}