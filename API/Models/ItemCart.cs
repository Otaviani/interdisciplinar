﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using API.Shared;
namespace API.Models
{
	public class ItemCart 
	{
		public string cart_uuid { get; set; }
		public string book_uuid { get; set; }
		public int quantity { get; set; }
		public decimal price { get; set; }

		public ItemCart(){
			
		}

	}
}
