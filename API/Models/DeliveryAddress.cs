﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using API.Shared;

namespace API.Models
{
	public class DeliveryAddress
	{
		public string uuid { get; set; }
		public string reader_people_uuid { get; set; }
		public string number { get; set; }
		public string cep { get; set; }
		public string address { get; set; }
		public string neighborhood { get; set; }
		public string city { get; set; }
		public string complement { get; set; }
		public string state_address { get; set; }
		public DeliveryAddress()
		{
		}
		public DeliveryAddress(FromFormCreateReader readerForm)
		{
			 number = readerForm.number;
			 cep = readerForm.cep;
			 address = readerForm.street;
			 neighborhood = readerForm.neighborhood;
			 city = readerForm.city;
			 complement = readerForm.complement;
			 state_address = readerForm.state;
		}
		public DeliveryAddress(FromFormCreateEmployee employeeForm)
		{
			 number = employeeForm.number;
			 cep = employeeForm.cep;
			 address = employeeForm.street;
			 neighborhood = employeeForm.neighborhood;
			 city = employeeForm.city;
			 complement = employeeForm.complement;
			 state_address = employeeForm.state;
		}
		public DeliveryAddress(SqlDataReader result)
		{
			uuid 				= (string) result["addresses_uuid"];
			reader_people_uuid 	= (string) result["people_uuid"];
			number  			= (string) result["number"];
			cep 				= (string) result["cep"];
			address 			= (string) result["address"];
			neighborhood 		= (string) result["neighborhood"];
			city 				= (string) result["city"];
			complement 			= (string) result["complement"];
			state_address 		= (string) result["state_address"];
		}

	}
}
