using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

using API.Shared;

namespace API.Models
{
    public class ReportSalesMonth
    {
		public int sold_books;
        public Decimal revenue;
        public int new_books;
	
        public ReportSalesMonth()
        {

        }

		public ReportSalesMonth(SqlDataReader result)
        {
            sold_books   = (int)     result["sold_books"];
            revenue      = (Decimal) result["revenue"];
            new_books    = (int)     result["new_books"];
        }
    }
}