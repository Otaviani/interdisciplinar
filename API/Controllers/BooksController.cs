﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Linq;

using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using API.Services;
using API.Models;
using API.Policies;
using API.Shared;

namespace API.Controllers
{
	[Route("api")]
	public class BooksController : ControllerBase
	{	
		public BooksController()
		{
		}

		[HttpGet]		
		[Route("genres")]
		[AllowAnonymous]
		public ActionResult<dynamic> GetGenres()
		{
			BookService service = new BookService();
			try{
				List<String> genres = service.GetGenres();
				return Ok(genres);
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}
		
		[HttpGet]
		[Route("settings/reports")]
		[Authorize]
		public ActionResult<dynamic> getReportSales()
		{
			string authHeader = Request.Headers["Authorization"];
			BooksPolicies policy = new BooksPolicies();
			BookService service = new BookService();
			try{
				policy.getReportSales(authHeader);
				dynamic report = service.GetReportSalesMonthCompare();
				report = filterReportSalesMonthCompare(report);
				return Ok(report);
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}

		public dynamic filterReportSalesMonthCompare(dynamic pReport){
			List<dynamic> reports = new List<dynamic>();
			foreach(ReportCompareSalesBooks element in pReport.reportSales){
				reports.Add(new {
						uuid = element.uuid,
						name = element.name,
						sales = element.sales,
						last_month_sales = element.last_month_sales,
						sales_diff = element.sales_diff,
						gain = element.gain,
						last_month_gain = element.last_month_gain,
						gain_diff = element.gain_diff
					}
				);
			}
			return new {
				sold_books =pReport.reportSalesMonth.sold_books,
				revenue = pReport.reportSalesMonth.revenue,
				last_month_revenue = pReport.reportSalesMonth.last_month_revenue,
				new_books = pReport.reportSalesMonth.new_books,
				reports
			};
		}

		[HttpPost]
		[Route("settings/books/create")]
		[Authorize]
		public ActionResult<dynamic> FormDataUpload([FromForm] FromFormBook FormBook)
		{
			string authHeader = Request.Headers["Authorization"];
			BooksPolicies policy = new BooksPolicies();
			BookService service = new BookService();
			Book book = new Book(FormBook);
			try{
				string UuidEmployee =  Services.TokenService.decodeToken(authHeader);
				policy.FormDataUpload(authHeader);
				service.newBook(book,UuidEmployee);
				return Ok();
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}

		[HttpPut]
		[Route("settings/books/{pUuid}/update")]
		[Authorize]
		public ActionResult<dynamic> UpdateBook([FromForm] FromFormBook FormBook ,string pUuid)
		{
			string authHeader = Request.Headers["Authorization"];
			BooksPolicies policy = new BooksPolicies();
			BookService service = new BookService();
			Book book = new Book(FormBook);
			try{
				string UuidEmployee =  Services.TokenService.decodeToken(authHeader);
				policy.FormDataUpload(authHeader);
				book.uuid = pUuid;
				book.employee_people_uuid = UuidEmployee;
				service.UpdateBook(book);
				return Ok();
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}

		[HttpPost]
		[Route("settings/find-book")]
		[Authorize]
		public ActionResult<dynamic> FindBookByIsbn([FromBody] Book book){
			string authHeader = Request.Headers["Authorization"];
			BooksPolicies policy = new BooksPolicies();
			GoogleApiBookService service = new GoogleApiBookService();
			try{
				policy.FindBookByIsbn(authHeader);
				Book bookInfos = service.GetBookInfo(book.isbn);
				return Ok(new {
					isbn               = bookInfos.isbn,
                	name               = bookInfos.name,
                	pages              = bookInfos.pages,
                	author             = bookInfos.author,
                	publishing_company = bookInfos.publishing_company,
                	synopsis           = bookInfos.synopsis
				});
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}


		[HttpPatch]
		[Route("settings/books/{pUuid}/update-quantity")]
		[Authorize]
		public ActionResult<dynamic> UpdateQuantityBook(String pUuid, [FromBody] Book book){
			string authHeader = Request.Headers["Authorization"];
			BooksPolicies policy = new BooksPolicies();
			BookService service = new BookService();
			try{
				policy.UpdateQuantityBook(authHeader);
				service.UpdateQuantityBook(pUuid, book.quantity);
				return Ok();
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}

		[HttpPatch]
		[Route("settings/books/{pUuid}/update-price")]
		[Authorize]
		public ActionResult<dynamic> UpdatePriceBook(String pUuid, [FromBody] Book book){
			string authHeader = Request.Headers["Authorization"];
			BooksPolicies policy = new BooksPolicies();
			BookService service = new BookService();
			try{
				policy.UpdatePriceBook(authHeader);
				service.UpdatePriceBook(pUuid, book.price);
				return Ok();
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}

		[HttpGet]
		[Route("books/{pUuid}")]
        [AllowAnonymous]
		public ActionResult<dynamic> GetBookByUUID(string pUuid){
			try{
				BookService service = new BookService();
				Book book = service.GetBookByUuid(pUuid);
				return Ok(
					new {
						uuid		=	book.uuid,
						name		=	book.name,
						isbn 		=	book.isbn,
						cover		=	book.cover,
						genre		=	book.genre,
						author		=	book.author,
						synopsis	=	book.synopsis,
						price		=	book.price,
						quantity 	=	book.quantity,
						pages		=	book.pages,
						publishing_company = book.publishing_company
                	}
				);
			}catch(Exception ex){
				return NotFound(new {Message = ex.Message});
            }
		}

		[HttpGet]
        [Route("settings/books")]
        [Authorize]
		public ActionResult<dynamic> GetListBooksSettings(){
			string authHeader = Request.Headers["Authorization"];
			BooksPolicies policy = new BooksPolicies();
			BookService service = new BookService();

			String filter = Request.Query["filter"];
			String gender = Request.Query["gender"];
				
			try{
				policy.GetListBooksSettings(authHeader);
				List<Book> listBooksSettings = service.GetListBooksSettings();
				return Ok(FilterlistBooksSettingsRetort(listBooksSettings,filter,gender));

			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message});
            }
		}
		public List<dynamic> FilterlistBooksSettingsRetort(List<Book> pRetort, String filter, String gender){
			List<Book> filteredReport = pRetort;
			List<dynamic> report = new List<dynamic>();

			if(filter != null){
				List<Book> aux = new List<Book>();
				foreach (Book book in filteredReport){
					if(book.name.Contains(filter))
						aux.Add(book);
				}
				filteredReport = aux;
			}

			if(gender != null){
				List<Book> aux = new List<Book>();
				foreach (Book book in filteredReport){
					if(book.genre.Equals(gender))
						aux.Add(book);
				}
				filteredReport = aux;
			}
			
			foreach (Book book in filteredReport)
			{
				report.Add(
					new{
						uuid	 = book.uuid,
						isbn 	 = book.isbn,
						name 	 = book.name,
						synopsis = book.synopsis,
						cover	 = book.cover,
						author 	 = book.author,
						price 	 = book.price,
						quantity = book.quantity
				});
			}
			return report;
		}

		[HttpGet]
        [Route("books")]
        [AllowAnonymous]
		public ActionResult<dynamic> GetListBooksToBuy(){
			try{

				BookService service = new BookService();

				String filter = Request.Query["filter"];
				String gender = Request.Query["gender"];
				
				List<Book> booksToBuy = service.GetListBooksToBuy();

				return Ok(FilterListBooksToBuyRetort(booksToBuy,filter,gender));

			}catch(Exception ex){
                return Unauthorized(new {Message = ex.Message});
            }
		}

		public List<dynamic> FilterListBooksToBuyRetort(List<Book> pRetort, String filter, String gender){
			List<Book> filteredReport = pRetort;
			List<dynamic> report = new List<dynamic>();

			if(filter != null){
				List<Book> aux = new List<Book>();
				foreach (Book book in filteredReport){
					if(book.name.Contains(filter))
						aux.Add(book);
				}
				filteredReport = aux;
			}

			if(gender != null){
				List<Book> aux = new List<Book>();
				foreach (Book book in filteredReport){
					if(book.genre.Equals(gender))
						aux.Add(book);
				}
				filteredReport = aux;
			}
			
			foreach (Book book in filteredReport)
			{
				report.Add(
					new{
						uuid	 = book.uuid,
						name	 = book.name,
						cover	 = book.cover,
						author	 = book.author,
						price	 = book.price,
						quantity = book.quantity
				});
			}
			return report;
		}

		[HttpDelete]
        [Route("settings/books/{uuid}/delete")]
        [Authorize]
		public ActionResult<dynamic> UpdateBook(string uuid)
		{
			string authHeader = Request.Headers["Authorization"];
			EmployeesPolicies policy = new EmployeesPolicies();
			BookService service = new BookService();
			try{
				policy.isAdministrator(authHeader);
				service.DeleteBook(uuid);
				return Ok();
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}
	}
}
