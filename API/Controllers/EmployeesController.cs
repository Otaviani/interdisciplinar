﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Linq;

using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using API.Services;
using API.Models;
using API.Policies;
using API.Shared;

namespace API.Controllers
{
	[Route("api")]
	public class EmployeesController : ControllerBase
	{
		public EmployeesController()
		{
			
		}

		[HttpDelete]
		[Route("settings/employees/{pUuid}/delete")]
	    [Authorize]
		public ActionResult<dynamic> DeleteEmployee(string pUuid ){
			String authHeader = Request.Headers["Authorization"];
			EmployeesPolicies policy = new EmployeesPolicies();
			EmployeeService service = new EmployeeService();
			try{
				policy.isAdministrator(authHeader);
				service.DeleteEmployee(pUuid);
				return Ok();
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message});
            }
		}

		[HttpPut]
        [Route("settings/employees/{pUuid}/update")]
        [Authorize]
		public ActionResult<dynamic> UpdateEmployee([FromForm] Employee employeeUpdate,string pUuid ){
			String authHeader = Request.Headers["Authorization"];
			EmployeesPolicies policy = new EmployeesPolicies();
			EmployeeService service = new EmployeeService();
			try{
				employeeUpdate.uuid = pUuid;
				policy.isAdministrator(authHeader);
				service.UpdateEmployee(employeeUpdate);
				return Ok();
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message});
            }
		}


		[HttpPost]
        [Route("settings/employee/create")]
        [Authorize]
		public ActionResult<dynamic>  createEmployee([FromForm] FromFormCreateEmployee employeeForm)
		{
			String authHeader = Request.Headers["Authorization"];
			EmployeesPolicies 	policy 	= new EmployeesPolicies();
			EmployeeService 	service = new EmployeeService();
			try{

				DeliveryAddress deliveryAddress = new DeliveryAddress(employeeForm);	
				Employee 		employee 		= new Employee(employeeForm);
				
				policy.isAdministrator(authHeader);
				service.Create(employee,deliveryAddress);

				return Ok(new {message = "Cadastro realizado com sucesso!" } );
			}catch(Exception ex){
				return Unauthorized( new{	message = ex.Message } );
			}
		}


		[HttpGet]
        [Route("settings/employees/{pUuid}")]
        [Authorize]
		public ActionResult<dynamic> GetEmployeebyUuid(string pUuid){
			String authHeader = Request.Headers["Authorization"];
			EmployeesPolicies policy = new EmployeesPolicies();
			EmployeeService service = new EmployeeService();
			try{
				policy.isEmployee(authHeader);
				Employee employee = service.GetEmployeeByUuid(pUuid);
				return Ok( new{
						uuid  = employee.uuid,
						name  = employee.name,
						cpf	  = employee.cpf,
						employee_type = employee.employee_type,
						email = employee.email,
						phone = employee.telephone,
						birth = employee.birth,
						photo = employee.photo
					}
				);
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message});
            }
		}

		[HttpGet]
        [Route("settings/employees")]
        [Authorize]
		public ActionResult<dynamic> GetAllEmployees(){
			String authHeader = Request.Headers["Authorization"];
			EmployeesPolicies policy = new EmployeesPolicies();
			EmployeeService service = new EmployeeService();
			try{
				policy.isEmployee(authHeader);
				List<Employee> listEmployees = service.GetAllEmployees();
				return Ok(FilterListEmployeesSettingsRetort(listEmployees));
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message});
            }
		}
		public List<dynamic> FilterListEmployeesSettingsRetort(List<Employee> pRetort){
			List<dynamic> filteredReport = new List<dynamic>();
			foreach (Employee employee in pRetort)
			{
				filteredReport.Add(
					new{
						uuid	= employee.uuid,
						name 	= employee.name,
						photo	= employee.photo,
						type	= employee.employee_type
				});
			}
			return filteredReport;
		}

	}
}
