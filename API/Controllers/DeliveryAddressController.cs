﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Linq;

using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using API.Services;
using API.Models;
using API.Shared;
using API.Policies;

namespace API.Controllers

{	[Route("api")]
	public class DeliveryAddressController : ControllerBase
	{
		public DeliveryAddressController()
		{
		}
		[HttpDelete]
		[Route("reader/{uuidAddress}/delete")]
		[Authorize]
		public ActionResult<dynamic> DeleteAddress(String uuidAddress){
			DeliveryAddressService service = new DeliveryAddressService();
			string authHeader = Request.Headers["Authorization"];
			ReaderPolices readerPolices = new ReaderPolices();
			try{
				DeliveryAddress delivery = service.GetAddressesByUuid(uuidAddress);
				readerPolices.AlterDataDeleteAddress(authHeader,delivery.reader_people_uuid);
				service.Delete(uuidAddress);
				return Ok();
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}

		[HttpPost]
		[Route("address/create")]
		[Authorize]
		public ActionResult<dynamic> CreateAddress([FromBody] DeliveryAddress pAddress){
			DeliveryAddressService service = new DeliveryAddressService();
			string authHeader = Request.Headers["Authorization"];
			ReaderPolices readerPolices = new ReaderPolices();
			try{
				string UuidEmployee =  Services.TokenService.decodeToken(authHeader);
				readerPolices.isReader(UuidEmployee);
				pAddress.reader_people_uuid = UuidEmployee;
				DeliveryAddress addressWin = service.Create(pAddress);
				return Ok(new{
						uuid	 	 = addressWin.uuid,
						number 	 	 = addressWin.number,
						cep			 = addressWin.cep,
						neighborhood = addressWin.neighborhood,
						complement 	 = addressWin.complement,
						city 		 = addressWin.city,
						state	 	 = addressWin.state_address,
						street	  	 = addressWin.address
				});
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}

		[HttpPut]
		[Route("addresses/{pUuidAddress}/update")]
		[Authorize]
		public ActionResult<dynamic> updateAddresses(string pUuidAddress,[FromBody] DeliveryAddress pAddress){
			DeliveryAddressService service 	= new DeliveryAddressService();
			string authHeader 				= Request.Headers["Authorization"];
			ReaderPolices readerPolices 	= new ReaderPolices();
			//DeliveryAddressPolices DeliveryAddress 	= new DeliveryAddressPolices();
			try{
				string UuidEmployee =  Services.TokenService.decodeToken(authHeader);
				readerPolices.isReader(UuidEmployee);
				
				service.Create(pAddress);
				return Ok();
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}

		[HttpGet]
		[Route("register/via-cep/{cep}")]
		[AllowAnonymous]
		public ActionResult<dynamic> FindAddressByViaCep(string cep){
			try{
				ViaCepService service = new ViaCepService();
				DeliveryAddress address = service.GetAddress(cep);
				return Ok(new {
					cep = address.cep,
                	city = address.city,
                	neighborhood = address.neighborhood,
                	state_address = address.state_address,
                	address = address.address
				});
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}

		[HttpGet]
		[Route("reader/{readerUuid}/addresses")]
		[Authorize]
		public ActionResult<dynamic> GetAddressesByReader(String readerUuid ){
			ReaderPolices ReaderPolices = new ReaderPolices();
			DeliveryAddressService service = new DeliveryAddressService();
			try{

				string authHeader = Request.Headers["Authorization"];
				ReaderPolices.GetAddressesByReader(authHeader,readerUuid);
				List<DeliveryAddress>  addresses = service.GetAddressesByReaderUuid(readerUuid);
				return Ok(FilterlistAddresses(addresses));

			}catch(Exception ex){
				return Unauthorized(new { Message = ex.Message });
			}
		}
		public List<dynamic> FilterlistAddresses(List<DeliveryAddress>  pAddresses){
			List<dynamic> filteredReport = new List<dynamic>();
			foreach (DeliveryAddress addresses in pAddresses)
			{
				filteredReport.Add(
					new{
						uuid	 = addresses.uuid,
						number = addresses.number,
						cep		= addresses.cep,
						neighborhood = addresses.neighborhood,
						complement = addresses.complement,
						city = addresses.city,
						state	 = addresses.state_address,
						street	 = addresses.address
				});
			}
			return filteredReport;
		}

	}
}
