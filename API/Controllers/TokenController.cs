using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using API.Services;
using API.Models;
using API.Shared;

namespace API.Controllers
{
    [Route("api")]
    public class TokenController : ControllerBase
    {
        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public ActionResult<dynamic> Authenticate([FromBody] Reader request)
        {
            try{ 
                ReaderService service = new ReaderService();
                EmployeeService serviceEmployee = new EmployeeService();
                
                Reader people = service.GetReaderByLoginPassword(request.login, request.password);
                Boolean isEmployee = serviceEmployee.IsEmployee(people.uuid);

                return Ok(new { token = TokenService.GenerateToken(people,isEmployee)  });

            }catch(Exception ex){
                return Unauthorized(new {Message = ex.Message});
            }
        }


        [HttpGet]
        [Route("test/Token")]
        [Authorize]
        public ActionResult<dynamic> testToken()
        {
            try{
                string authHeader = Request.Headers["Authorization"];
                return Ok(new { 
                              uuid = TokenService.decodeToken(authHeader),
                            });
            }catch(Exception ex){
                return Unauthorized(new {Message = ex.Message});
            }
        }
        
    }


}