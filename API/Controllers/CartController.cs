﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Linq;

using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using API.Services;
using API.Models;
using API.Policies;
using API.Shared;

namespace API.Controllers
{
	[Route("api")]
	public class CartController : ControllerBase
	{
		public CartController()
		{
		}
		[HttpPost]
		[Route("reader/finish-purchase")]
		[Authorize]
		public ActionResult<dynamic> buyCart([FromBody] RequestToBuyCart pRequest ){

			CartService cartService = new CartService();
			string authHeader = Request.Headers["Authorization"];
            string readerUuid = TokenService.decodeToken(authHeader);
			try{
				List<BookToBuy> itensCart = pRequest.BooksToBuy;
				cartService.buyCart(pRequest.address_uuid,itensCart);
				return Ok();
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}

		[HttpPost]
		[Route("reader/cart-items")]
		[Authorize]
		public ActionResult<dynamic> GetListBooks([FromBody] List<String> pListUuid ){

			BookService service = new BookService();
			List<dynamic> listBooks = new List<dynamic>();
			try{
				foreach(String BookUuid in pListUuid){
					Book book = service.GetBookByUuid(BookUuid);
					listBooks.Add(
						new{
							uuid =book.uuid,
							name =book.name,
							synopsis = book.synopsis,
							cover = book.cover,
							genre =book.genre,
							price = book.price,
							quantity = book.quantity
						}
					);
				}
				return Ok(listBooks);
			}catch(Exception ex){
				return Unauthorized(new {Message = ex.Message });
			}
		}
		

	}
}
