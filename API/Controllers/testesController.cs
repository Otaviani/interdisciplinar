using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Linq;

using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using API.Services;
using API.Models;
using API.Policies;
using API.Shared;

namespace API.Controllers
{
	[Route("api/testes")]
	public class testesController : ControllerBase
	{	
		public testesController()
		{
		}

		[HttpPost]
		[Route("sendfile")]
		[AllowAnonymous]
		public ActionResult<dynamic> FormDataUpload([FromForm] Book book)
		{
            FilesService.uploudFile("books", book.coverFile);
			return Ok();
		}
	}
}
