﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using API.Services;
using API.Models;
using API.Policies;
using API.Shared;

namespace API.Controllers
{
	[Route("api")]
	public class ReaderController : ControllerBase
	{
		public ReaderController()
		{
			
		}


		[HttpPatch]
		[Route("reader/{uuid}/update-description")]
		[Authorize]
		public ActionResult<dynamic> updateDescription([FromBody] FromFormCreateReader readerForm, string uuid)
		{
			ReaderPolices polices = new ReaderPolices();
			ReaderService service = new ReaderService();
			EmployeeService serviceEmployee = new EmployeeService();
			string authHeader = Request.Headers["Authorization"];

			try{
				polices.AlterDataPatch(authHeader, uuid);
				Reader reader = service.GetReaderByUuid(uuid);
				reader.about= readerForm.about;
				reader.uuid=uuid;
				service.Update(reader);
				Reader people = service.GetReaderByUuid(uuid);
                Boolean isEmployee = serviceEmployee.IsEmployee(people.uuid);

                return Ok(new { token = TokenService.GenerateToken(people,isEmployee)  });
			}catch(Exception ex){
				return new{	message = ex.Message};
			}
		}
		
		[HttpPatch]
		[Route("reader/{uuid}/update-number")]
		[Authorize]
		public ActionResult<dynamic> updateNumber([FromBody] FromFormCreateReader readerForm, string uuid)
		{
			ReaderPolices polices = new ReaderPolices();
			ReaderService service = new ReaderService();
			EmployeeService serviceEmployee = new EmployeeService();
			string authHeader = Request.Headers["Authorization"];

			try{
				polices.AlterDataPatch(authHeader, uuid);
				Reader reader = service.GetReaderByUuid(uuid);
				reader.telephone= readerForm.telephone;
				reader.uuid=uuid;
				service.Update(reader);
				Reader people = service.GetReaderByUuid(uuid);
                Boolean isEmployee = serviceEmployee.IsEmployee(people.uuid);

                return Ok(new { token = TokenService.GenerateToken(people,isEmployee)  });
			}catch(Exception ex){
				return new{	message = ex.Message};
			}
		}
		
		[HttpPatch]
		[Route("reader/{uuid}/update-email")]
		[Authorize]
		public ActionResult<dynamic> updateEmail([FromBody] FromFormCreateReader readerForm, string uuid)
		{
			ReaderPolices polices = new ReaderPolices();
			ReaderService service = new ReaderService();
			EmployeeService serviceEmployee = new EmployeeService();
			string authHeader = Request.Headers["Authorization"];

			try{
				polices.AlterDataPatch(authHeader, uuid);
				Reader reader = service.GetReaderByUuid(uuid);
				reader.email= readerForm.email;
				reader.uuid=uuid;
				service.Update(reader);
				Reader people = service.GetReaderByUuid(uuid);
                Boolean isEmployee = serviceEmployee.IsEmployee(people.uuid);

                return Ok(new { token = TokenService.GenerateToken(people,isEmployee)  });
			}catch(Exception ex){
				return new{	message = ex.Message};
			}
		}



		[HttpPatch]
		[Route("reader/{uuid}/update-name")]
		[Authorize]
		public ActionResult<dynamic> updateName([FromBody] FromFormCreateReader readerForm, string uuid)
		{
			ReaderPolices polices = new ReaderPolices();
			ReaderService service = new ReaderService();
			EmployeeService serviceEmployee = new EmployeeService();
			string authHeader = Request.Headers["Authorization"];

			try{
				polices.AlterDataPatch(authHeader, uuid);
				Reader reader = service.GetReaderByUuid(uuid);
				reader.name= readerForm.name;
				reader.uuid=uuid;
				service.Update(reader);
				Reader people = service.GetReaderByUuid(uuid);
                Boolean isEmployee = serviceEmployee.IsEmployee(people.uuid);

                return Ok(new { token = TokenService.GenerateToken(people,isEmployee)  });
			}catch(Exception ex){
				return new{	message = ex.Message};
			}
		}



		[HttpPatch]
		[Route("reader/{uuid}/update-photo")]
		[Authorize]
		public ActionResult<dynamic> updatePhoto([FromForm] FromFormCreateReader readerForm, string uuid)
		{
			
			ReaderPolices polices = new ReaderPolices();
			ReaderService service = new ReaderService();
			EmployeeService serviceEmployee = new EmployeeService();
			string authHeader = Request.Headers["Authorization"];

			try{
				polices.AlterDataPatch(authHeader, uuid);
				Reader reader = service.GetReaderByUuid(uuid);
				reader.photoFile= readerForm.photoFile;
				reader.uuid=uuid;
				service.Update(reader);
				Reader people = service.GetReaderByUuid(uuid);
                Boolean isEmployee = serviceEmployee.IsEmployee(people.uuid);

                return Ok(new { token = TokenService.GenerateToken(people,isEmployee)  });
			}catch(Exception ex){
				return new{	message = ex.Message};
			}
		}
		[HttpPost]
        [Route("reader/create")]
        [AllowAnonymous]
		public ActionResult<dynamic>  createReader([FromForm] FromFormCreateReader readerForm)
		{
			DeliveryAddress deliveryAddress = new DeliveryAddress(readerForm);	
			Reader reader = new Reader(readerForm);
			try{
				ReaderService service = new ReaderService();
				service.Create(reader,deliveryAddress);
				return Ok(new {message = "Cadastro realizado com sucesso!" } );
			}catch(Exception ex){
				return new{	message = ex.Message};
			}
		}

		[HttpGet]
        [Route("reader")]
        [Authorize]
		public ActionResult<dynamic> GetReader()
		{
			ReaderService service = new ReaderService();
			Reader reader = new Reader();
			string authHeader = Request.Headers["Authorization"];
			try{
				reader.uuid = Services.TokenService.decodeToken(authHeader);
				reader = service.GetReaderByUuid(reader.uuid);

				return Ok(
					new{
						uuid = reader.uuid,
         				name = reader.name,
	     				cpf = reader.cpf,
	     				birth = reader.birth,
         				photo = reader.photo,
						telephone = reader.telephone,
						email = reader.email,
						about = reader.about
					});
			}catch(Exception ex){
				return NotFound( new{	message = ex.Message} );
			}
		}

	}
}
