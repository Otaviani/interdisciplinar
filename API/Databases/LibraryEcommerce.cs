using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;

using API.Shared;

namespace API.Databases
{
    public abstract class LibraryEcommerce : ConnectionSqlServer
    {  
        public LibraryEcommerce(){
            builder.ConnectTimeout = 5000;
            builder.DataSource = "localhost";
            builder.InitialCatalog = "library_ecommerce";
            builder.UserID = "sa";
            builder.Password = "our_strong#very(hard)password~with[Ç]forNOtoBEhacked!";
        }
    }
}
