﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using API.Models;
using API.Databases;

namespace API.Data
{
	public class CartData: LibraryEcommerce
	{
		public CartData()
		{
		}

		public void upsertCart(Cart pCart){
			String nameProcedure = "upsertCart";
			SqlCommand cmd = BuildSqlCommandTypeStoredProcedure(nameProcedure);
			cmd.Parameters.AddWithValue("@uuid", pCart.uuid);
			cmd.Parameters.AddWithValue("@delivery_addresses_uuid", pCart.delivery_addresses_uuid);
			cmd.Parameters.AddWithValue("@bought_in", pCart.bought_in);
			cmd.Parameters.AddWithValue("@status_carts", pCart.status_carts);
			insertComand(cmd);
		}

		public void insterItemCart(ItemCart pItem){
			String nameProcedure = "upsertItemCart";
			SqlCommand cmd = BuildSqlCommandTypeStoredProcedure(nameProcedure);
			cmd.Parameters.AddWithValue("@cart_uuid",	pItem.cart_uuid);
			cmd.Parameters.AddWithValue("@book_uuid",	pItem.book_uuid);
			cmd.Parameters.AddWithValue("@quantity",	pItem.quantity);
			cmd.Parameters.AddWithValue("@price", 		pItem.price);
			insertComand(cmd);
		}

	}
}
