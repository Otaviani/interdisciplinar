﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using API.Models;
using API.Databases;

namespace API.Data
{
	public class DeliveryAddressData : LibraryEcommerce
	{
		public DeliveryAddressData()
		{
		}

		public void Upsert(DeliveryAddress address){
			String nameProcedure = "upsertAddress";
			SqlCommand cmd = BuildSqlCommandTypeStoredProcedure(nameProcedure);

			cmd.Parameters.AddWithValue("@uuid", address.uuid);
			cmd.Parameters.AddWithValue("@reader_people_uuid", address.reader_people_uuid);
			cmd.Parameters.AddWithValue("@number", address.number);
			cmd.Parameters.AddWithValue("@cep", address.cep);
			cmd.Parameters.AddWithValue("@address", address.address);
			cmd.Parameters.AddWithValue("@neighborhood", address.neighborhood);
			cmd.Parameters.AddWithValue("@city", address.city);
			cmd.Parameters.AddWithValue("@complement", address.complement);
			cmd.Parameters.AddWithValue("@state_address", address.state_address);
			
			insertComand(cmd);
		}

		public List<DeliveryAddress> GetListDeliveryAddressDataByPeople_uuid(String pPeople_uuid){
			String sql = @"SELECT * FROM vwReaderAddresses WHERE people_uuid = @people_uuid AND deleted_at IS NULL";

			SqlCommand sqlCommand = new SqlCommand(sql);
			sqlCommand.Parameters.AddWithValue("@people_uuid", pPeople_uuid);

			return GenereteListDeliveryAddressData(sqlCommand);
		}

        public void Delete(string uuidAddress)
        {
         	String nameProcedure = "deleteAddress";
			SqlCommand cmd = BuildSqlCommandTypeStoredProcedure(nameProcedure);
			cmd.Parameters.AddWithValue("@uuid", uuidAddress);
			insertComand(cmd);
        }

        public List<DeliveryAddress> GenereteListDeliveryAddressData(SqlCommand pSqlCommand){

        	List<DeliveryAddress> list = new List<DeliveryAddress>();
			SqlDataReader result = ReadComand(pSqlCommand);
			while (result.Read()){
				list.Add(new DeliveryAddress(result));
			}
			return list;
		}

        public DeliveryAddress GetAddressesByUuid(string pUuid)
        {
            String sql = "SELECT * FROM vwReaderAddresses WHERE addresses_uuid=@uuid";
			SqlCommand sqlCommand = new SqlCommand(sql);
			sqlCommand.Parameters.AddWithValue("@uuid", pUuid);
			return OpenAddressesBySqlCommand(sqlCommand);
        }

		public DeliveryAddress OpenAddressesBySqlCommand(SqlCommand pSqlCommand)
		{
			SqlDataReader result = ReadComand(pSqlCommand);
            if (!result.Read())
				throw new System.InvalidOperationException("Registro não encontrado");	
			return new DeliveryAddress(result);
		}

    }
}
