﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using API.Models;
using API.Databases;


namespace API.Data
{
	public class BookData : LibraryEcommerce
	{
		public BookData() 
		{
			
		}
		public void upsertBook(Book pBook){

			String nameProcedure = "upsertBook";
			SqlCommand cmd = BuildSqlCommandTypeStoredProcedure(nameProcedure);

			cmd.Parameters.AddWithValue("@uuid", pBook.uuid);
			cmd.Parameters.AddWithValue("@employee_people_uuid", pBook.employee_people_uuid);
			cmd.Parameters.AddWithValue("@name", pBook.name);
			cmd.Parameters.AddWithValue("@author", pBook.author);
			cmd.Parameters.AddWithValue("@cover", pBook.cover);
			cmd.Parameters.AddWithValue("@pages", pBook.pages);
			cmd.Parameters.AddWithValue("@publishing_company", pBook.publishing_company);
			cmd.Parameters.AddWithValue("@quantity", pBook.quantity);
			cmd.Parameters.AddWithValue("@genre", pBook.genre);
			cmd.Parameters.AddWithValue("@price", pBook.price);
			cmd.Parameters.AddWithValue("@isbn", pBook.isbn);
			cmd.Parameters.AddWithValue("@synopsis", pBook.synopsis);
			
			insertComand(cmd);
		}
		public ReportSalesMonth slectReportSalesMonth(int pMonth, int pYear){
			String nameProcedure = "slectReportSalesMonth";
			SqlCommand cmd = BuildSqlCommandTypeStoredProcedure(nameProcedure);
			cmd.Parameters.AddWithValue("@month", pMonth);
			cmd.Parameters.AddWithValue("@year", pYear);
			SqlDataReader result = ReadComand(cmd);
			if (!result.Read()){
				throw new System.InvalidOperationException("Não encontrados Registros");	
			}
			return new ReportSalesMonth(result);
		}
		public List<ReportSalesBooks> GetReportSalesBooksByMonthAndYear(int pMonth, int pYear){
			String sql = @"SELECT 
								uuid,
								name,
								SUM(sales) As sales,
								SUM(gain) AS gain
							FROM
								vwReportSalesBooks 
							WHERE 
								datepart(month, bought_in) = @pMonth AND
								datepart(year, bought_in) = @pYear
							GROUP BY uuid, name
							ORDER By uuid";
			SqlCommand sqlCommand = new SqlCommand(sql);
			sqlCommand.Parameters.AddWithValue("@pMonth", pMonth);
			sqlCommand.Parameters.AddWithValue("@pYear", pYear);
			return GenereteListReportSalesBooks(sqlCommand);
		}
		public List<ReportSalesBooks> GenereteListReportSalesBooks(SqlCommand pSqlCommand){
        	List<ReportSalesBooks> ReportSalesMonthList = new List<ReportSalesBooks>();
			SqlDataReader result = ReadComand(pSqlCommand);
			while (result.Read()){
				ReportSalesMonthList.Add(new ReportSalesBooks(result));
			}
			return ReportSalesMonthList;
		}

        public List<string> GetGenres(){
            SqlCommand sqlCommand = new SqlCommand("SELECT genre FROM vwGetAllBooks GROUP BY genre");
            SqlDataReader result = ReadComand(sqlCommand);
			List<string> list = new List<string>();
			while(result.Read()){
				String genre = (String) result["genre"];
				list.Add(genre);
			}
			return list;
        }

        public void UpdatePriceBook(String pUuid, Decimal pPrice){
			String nameProcedure = "updatePriceBook";
			SqlCommand cmd = BuildSqlCommandTypeStoredProcedure(nameProcedure);
			cmd.Parameters.AddWithValue("@uuid", pUuid);
			cmd.Parameters.AddWithValue("@price", pPrice);
			insertComand(cmd);
		}
		public void UpdateQuantityBook(String pUuid, int pQuantity){
			String nameProcedure = "updateQuantityBook";
			SqlCommand cmd = BuildSqlCommandTypeStoredProcedure(nameProcedure);
			cmd.Parameters.AddWithValue("@uuid", pUuid);
			cmd.Parameters.AddWithValue("@quantity", pQuantity);
			insertComand(cmd);
		}

        public bool BookExistsByUuid(String pUuid)
        {
			SqlCommand sqlCommand = new SqlCommand("Select * From vwGetAllBooks WHERE uuid = @uuid");
			sqlCommand.Parameters.AddWithValue("@uuid", pUuid);
            SqlDataReader result = ReadComand(sqlCommand);
			return result.Read();
        }

        public Book GetBookByUuid(String pUuid){
			SqlCommand sqlCommand = new SqlCommand("Select * From vwGetAllBooks WHERE uuid = @uuid");
			sqlCommand.Parameters.AddWithValue("@uuid", pUuid);
			return OpenBookBySqlCommand(sqlCommand);
		}

		public List<Book> GetAll(){
			SqlCommand sqlCommand = new SqlCommand("Select * From vwGetAllBooks WHERE deleted_at IS NULL ORDER BY created_at DESC");
			return GenereteListBookBySqlCommand(sqlCommand);
		}

        public bool BookExistsByIsbnAndNotIsUuid(string pIsbn, string pUuid)
        {
            SqlCommand sqlCommand = new SqlCommand("Select * From vwGetAllBooks WHERE isbn = @isbn AND uuid <> @uuid");
			sqlCommand.Parameters.AddWithValue("@isbn", pIsbn);
			sqlCommand.Parameters.AddWithValue("@uuid", pUuid);
			SqlDataReader result = ReadComand(sqlCommand);
			return result.Read();
        }

        public List<Book> GetLast5BookReleases()
		{
			SqlCommand sqlCommand = new SqlCommand("Select * From vwLast5BookReleases ");
			return GenereteListBookBySqlCommand(sqlCommand);
		}

		public List<Book> GetBooksWithStock()
		{
			SqlCommand sqlCommand = new SqlCommand("Select * From vwBooksWithStock ORDER BY updated_at DESC, created_at DESC");
			return GenereteListBookBySqlCommand(sqlCommand);
		}

		public List<Book> GetBooksWithoutStock()
		{
			SqlCommand sqlCommand = new SqlCommand("Select * From vwBooksWithoutStock ORDER BY updated_at DESC, created_at DESC");
			return GenereteListBookBySqlCommand(sqlCommand);
		}

		public List<Book> GenereteListBookBySqlCommand(SqlCommand pSqlCommand){
        	List<Book> BookList = new List<Book>();
			SqlDataReader result = ReadComand(pSqlCommand);
			while (result.Read()){
				BookList.Add(new Book(result));
			}
			return BookList;
		}
		public Book OpenBookBySqlCommand(SqlCommand pSqlCommand)
		{
			SqlDataReader result = ReadComand(pSqlCommand);
            if (!result.Read())
				throw new System.InvalidOperationException("Registro não encontrado");	
			return new Book(result);
		}

        internal void DeleteBook(string pUuid)
        {
			SqlCommand sqlCommand = new SqlCommand("UPDATE books SET deleted_at = GETDATE() WHERE books.uuid=@uuid");
			sqlCommand.Parameters.AddWithValue("@uuid", pUuid);
			insertComand(sqlCommand);
        }
    }
}
