﻿using System;
using System.Data;
using System.Data.SqlClient;
using API.Models;
using API.Databases;

namespace API.Data
{
	public class ReaderData : LibraryEcommerce
	{
		public ReaderData()
		{
		}

		public void upsertReader(Reader pReaderModel){

			String nameProcedure = "upsertReader";
			SqlCommand cmd = BuildSqlCommandTypeStoredProcedure(nameProcedure);

			cmd.Parameters.AddWithValue("@uuid", pReaderModel.uuid);
			cmd.Parameters.AddWithValue("@people_name",pReaderModel.name );
			cmd.Parameters.AddWithValue("@cpf", pReaderModel.cpf);
			cmd.Parameters.AddWithValue("@birth", pReaderModel.birth);
			cmd.Parameters.AddWithValue("@photo", pReaderModel.photo);
			cmd.Parameters.AddWithValue("@people_login", pReaderModel.login);
			cmd.Parameters.AddWithValue("@people_password", pReaderModel.password);
			cmd.Parameters.AddWithValue("@telephone", pReaderModel.telephone);
			cmd.Parameters.AddWithValue("@email", pReaderModel.email);
			cmd.Parameters.AddWithValue("@about", pReaderModel.about);
			
			insertComand(cmd);
		}

        public bool ExistReader(string pUuid)
        {
			SqlCommand sqlCommand = new SqlCommand("SELECT * From vwReader WHERE uuid = @uuid");
			sqlCommand.Parameters.AddWithValue("@uuid", pUuid);
			SqlDataReader result = ReadComand(sqlCommand);
			return result.Read();
        }

        public Reader GetReaderByUuid(String pUuid)
		{
			SqlCommand sqlCommand = new SqlCommand("SELECT * From vwReader WHERE uuid = @uuid");
			sqlCommand.Parameters.AddWithValue("@uuid", pUuid);
			return OpenReaderBySqlCommand(sqlCommand);
		}

		public Reader GetReaderByLoginPassword(String pPeople_login, String pPeople_password)
		{
			SqlCommand sqlCommand = new SqlCommand("SELECT * From vwReader WHERE (login=@login OR email=@login) AND password = @password");
			sqlCommand.Parameters.AddWithValue("@login", pPeople_login);
			sqlCommand.Parameters.AddWithValue("@password", pPeople_password);
			return OpenReaderBySqlCommand(sqlCommand);
		}
		public Reader OpenReaderBySqlCommand(SqlCommand pSqlCommand)
		{
			SqlDataReader result = ReadComand(pSqlCommand);

            if (!result.Read())
				throw new System.InvalidOperationException("Registro não encontrado");	

			return new Reader(result);
		}
	}
}
