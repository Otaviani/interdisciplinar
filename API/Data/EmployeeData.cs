﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using API.Models;
using API.Databases;

namespace API.Data
{
	public class EmployeeData : LibraryEcommerce
	{
		public EmployeeData()
		{
		}
		public void UpsertEmployee(Employee pEmployeeModel){

			String nameProcedure = "upsertEmployee";
			SqlCommand cmd = BuildSqlCommandTypeStoredProcedure(nameProcedure);

			cmd.Parameters.AddWithValue("@uuid", pEmployeeModel.uuid);
			cmd.Parameters.AddWithValue("@people_name",pEmployeeModel.name );
			cmd.Parameters.AddWithValue("@cpf", pEmployeeModel.cpf);
			cmd.Parameters.AddWithValue("@birth", pEmployeeModel.birth);
			cmd.Parameters.AddWithValue("@people_login", pEmployeeModel.login);
			cmd.Parameters.AddWithValue("@people_password", pEmployeeModel.password);
			cmd.Parameters.AddWithValue("@telephone", pEmployeeModel.telephone);
			cmd.Parameters.AddWithValue("@email", pEmployeeModel.email);
			cmd.Parameters.AddWithValue("@about", pEmployeeModel.about);
			cmd.Parameters.AddWithValue("@photo", pEmployeeModel.photo);
			cmd.Parameters.AddWithValue("@employee_type", pEmployeeModel.employee_type);
			cmd.Parameters.AddWithValue("@employee_status", pEmployeeModel.employee_status);

			insertComand(cmd);
		}

        internal bool ExistEmployeeAdministratorByUuid(string pUuid)
        {
           	SqlCommand sqlCommand = new SqlCommand("SELECT * From vwEmployees WHERE uuid = @uuid AND employee_type='administrador' And employee_status = 'ativo'");
			sqlCommand.Parameters.AddWithValue("@uuid", pUuid);
			SqlDataReader result = ReadComand(sqlCommand);
			return result.Read();
        }

        public List<Employee> GetAllEmployees()
		{
			SqlCommand sqlCommand = new SqlCommand("SELECT * From vwEmployees ORDER BY employee_type, name");
			return GenereteListEmployeesBySqlCommand(sqlCommand);
		}
		public Employee GetEmployeeByLoginPassword(String pPeople_login, String pPeople_password)
		{
			SqlCommand sqlCommand = new SqlCommand("SELECT * From vwEmployees WHERE login = @login AND password = @password");
			sqlCommand.Parameters.AddWithValue("@login", pPeople_login);
			sqlCommand.Parameters.AddWithValue("@password", pPeople_password);
			return OpenEmployeeBySqlCommand(sqlCommand);
		}
		public Employee GetEmployeeByUuid(string pUuid)
		{
			SqlCommand sqlCommand = new SqlCommand("SELECT * From vwEmployees WHERE uuid = @uuid");
			sqlCommand.Parameters.AddWithValue("@uuid", pUuid);
			return OpenEmployeeBySqlCommand(sqlCommand);
		}

		public Boolean ExistEmployeeActiveByUuid(string pUuid){
			SqlCommand sqlCommand = new SqlCommand("SELECT * From vwEmployees WHERE uuid = @uuid And employee_status = 'ativo'");
			sqlCommand.Parameters.AddWithValue("@uuid", pUuid);
			SqlDataReader result = ReadComand(sqlCommand);
			return result.Read();
		} 

		public Employee OpenEmployeeBySqlCommand(SqlCommand pSqlCommand)
		{
			SqlDataReader result = ReadComand(pSqlCommand);
            if (result.Read()){
				return new Employee(result);
			}            
			throw new System.InvalidOperationException("Registro não encontrado");
		}

		public List<Employee> GenereteListEmployeesBySqlCommand(SqlCommand pSqlCommand){
        	List<Employee> EmployeeList = new List<Employee>();
			SqlDataReader result = ReadComand(pSqlCommand);
			while (result.Read()){
				EmployeeList.Add(new Employee(result));
			}
			return EmployeeList;
		}


	}
}
