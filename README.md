# Requisitos

1-  docker instalado.

2 - dotnet instalado.


# Instalação do projeto:

## Back-end

Caso esteja no windows, certifique-se que os arquivos entrypoint.sh e configure-db.sh encontrados na raiz do projeto estão no formato LF.

Execute dento do diretório do projeto:

```
$ docker-compose up --build
```

Entre em API com o comando cd API/ e Execute:

```
$ dotnet run
```


OBS: caso não tenha Docker instalado, os arquivos .SQL estão no diretorio Database e as configurações para coneção em LibraryEcommerce.cs.



